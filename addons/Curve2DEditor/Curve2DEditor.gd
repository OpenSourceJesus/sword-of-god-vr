@tool
extends EditorPlugin

@export var curves : Array[_Curve2D]
static var meshInstance : MeshInstance2D

func Do ():
	curves.clear()
	curves.append_array(NodeExtensions.GetNodes(_Curve2D, false, get_parent()))
	for curve in curves:
		curve.Update ()
		var vertices = PackedVector3Array()
		for i in range(curve.curve.point_count):
			vertices.append(VectorExtensions.ToVec3(curve.curve.get_point_position(i)))
			for i2 in range(curve.extraChecksPerPoint):
				var sampleAt = float(i2 + 1) / (curve.extraChecksPerPoint + 1)
				vertices.append(VectorExtensions.ToVec3(curve.curve.sample(i, sampleAt)))
		var mesh = ImmediateMesh.new()
		mesh.surface_begin(Mesh.PRIMITIVE_TRIANGLES)
		var firstVertex = vertices[0]
		var previousVertex = firstVertex
		var currentCenter = curve.centers[0]
		var currentCenterIndex = 0
		for i in range(1, vertices.size()):
			var vertex = vertices[i]
			mesh.surface_add_vertex(vertex)
			mesh.surface_add_vertex(previousVertex)
			if currentCenter.endIndex > 0 && i + (curve.extraChecksPerPoint + 1) > currentCenter.endIndex:
				currentCenterIndex += 1
				currentCenter = curve.centers[currentCenterIndex]
			mesh.surface_add_vertex(VectorExtensions.ToVec3(currentCenter.global_position))
			previousVertex = vertex
		mesh.surface_add_vertex(VectorExtensions.ToVec3(currentCenter.global_position))
		mesh.surface_add_vertex(firstVertex)
		mesh.surface_add_vertex(previousVertex)
		mesh.surface_end()
		if meshInstance != null:
			meshInstance.free()
		meshInstance = MeshInstance2D.new()
		meshInstance.mesh = mesh
		meshInstance.global_position = EditorInterface.get_editor_viewport_2d().get_final_transform().origin
		get_parent().add_child(meshInstance)

func _enter_tree ():
	pass

func _exit_tree ():
	if meshInstance != null:
		meshInstance.free()

func _process (deltaTime : float):
	Do ()
