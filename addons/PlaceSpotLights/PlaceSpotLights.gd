@tool
extends EditorPlugin
class_name PlaceSpotLights

var data : PlaceSpotLightsData
var actOnScene : Node
var placePoints : Array[Node3D]
var meshInstances : Array[MeshInstance3D]
const DATA_SCENE_PATH = 'res://Scenes/Concepts (Scenes)/PlaceSpotLightsData.tscn'
const BIG_NUMBER = 99999
const RAY_CAST_LENGTH = BIG_NUMBER

func _process (deltaTime : float):
	data = load(DATA_SCENE_PATH).instantiate()
	if data.do:
		data.do = false
		GameManager.SaveNode (data, DATA_SCENE_PATH)
		Do ()

func Do ():
	print('Making lights')
	actOnScene = data.actOnScene.instantiate()
	meshInstances.clear()
	meshInstances.append_array(NodeExtensions.GetNodes(MeshInstance3D, false, actOnScene))
	var totalNoticability = 0.0
	for i in range(data.targetPlaceCount):
		for i2 in range(data.maxRetries):
			var placePointsNodes = actOnScene.find_child(data.placePointsParentName).get_children()
			for placePointNode in placePointsNodes:
				placePoints.append(placePointNode)
			var position = placePoints[randi_range(0, placePoints.size() - 1)].global_position
			var light = SpotLight3D.new()
			actOnScene.add_child(light)
			var forward = VectorExtensions.Random3D(FloatRange.New(1, 1))
			var contactPoint = Raycast(position, forward)
			if contactPoint == null || contactPoint.distance < data.distanceFromContactPointRange.min:
				light.free()
				continue
			light.light_energy = randf_range(data.intensityRange.min, data.intensityRange.max)
			light.spot_angle = randf_range(data.spotAngleRange.min, data.spotAngleRange.max)
			if data.colorMode == ColorMode.Gradient:
				light.light_color = data.validColorsGraidient.Evaluate(randf())
			elif data.colorMode == ColorMode.ColorPalette:
				light.light_color = data.validColorPalette.Get(randf())
			else:
				light.light_color = ColorExtensions.Random()
			var distance = contactPoint.distance
			var newDistance = randf_range(data.distanceFromContactPointRange.min, data.distanceFromContactPointRange.max)
			position += forward * distance - (forward * newDistance)
			light.spot_range = distance * randf_range(data.rangeMultiplierRange.min, data.rangeMultiplierRange.max)
			light.global_position = position
			light.look_at(light.global_position + forward)
			var noticability = GetNoticability(light, data.degreesPerRayCast)
			print('Noticability' + str(noticability))
			if noticability < data.eachLightNoticabilityRange.min || noticability > data.eachLightNoticabilityRange.max:
				light.free()
				continue
			var newTotalNoticability = totalNoticability + noticability
			if newTotalNoticability < data.totalNoticabilityRange.min || newTotalNoticability > data.totalNoticabilityRange.max:
				light.free()
				continue
			totalNoticability = newTotalNoticability
			print('Made light')
			break
	GameManager.SaveNode (actOnScene,  data.actOnScene.resource_path)

func Raycast (position : Vector3, direction : Vector3) -> ContactPoint:
	var output : ContactPoint =  null
	var minDistance = INF
	var lineSegment = LineSegment3D.New(position, position + direction * BIG_NUMBER)
	for meshInstance in meshInstances:
		var meshData = meshInstance.mesh.surface_get_arrays(0)
		var meshIndices = meshData[12]
		var meshVertices = meshData[0]
		var meshUvs = meshData[4]
		for i2 in range(0, meshIndices.size(), 3):
			var index = meshIndices[i2]
			var index2 = meshIndices[i2 + 1]
			var index3 = meshIndices[i2 + 2]
			var point = meshVertices[index] * meshInstance.get_parent_node_3d().scale
			var point2 = meshVertices[index2] * meshInstance.get_parent_node_3d().scale
			var point3 = meshVertices[index3] * meshInstance.get_parent_node_3d().scale
			var face = _Shape3DFace.FromCorners([point, point2, point3])
			var intersection = face.GetIntersectionPointWithLineSegment(lineSegment)
			if intersection != null:
				var distance = (intersection - position).length()
				if distance < minDistance:
					minDistance = distance
					var uv = meshUvs[index]
					var uv2 = meshUvs[index2]
					var uv3 = meshUvs[index3]
					var shaderMaterial = meshInstance.get_active_material(0) as ShaderMaterial
					if shaderMaterial != null:
						var image = shaderMaterial.get_shader_parameter('texture').get_image()
						var color = image.get_pixel(float(image.get_width()) / (1.0 / uv.x), float(image.get_height()) / (1.0 / uv.y))
						var color2 = image.get_pixel(float(image.get_width()) / (1.0 / uv2.x), float(image.get_height()) / (1.0 / uv2.y))
						var color3 = image.get_pixel(float(image.get_width()) / (1.0 / uv3.x), float(image.get_height()) / (1.0 / uv3.y))
						output = ContactPoint.New(distance, ColorExtensions.GetAverage([color, color2, color3]))
					else:
						output = ContactPoint.New(distance, Color.DIM_GRAY)
	return output

func GetContactPoints (light : SpotLight3D, degreesPerRayCast : float) -> Array:
	var up = VectorExtensions.Random3D(FloatRange.New(1, 1))
	var forward = light.to_global(Vector3.FORWARD) - light.global_position
	up = up.cross(forward)
	var contactPoints : Array[ContactPoint]
	var rayCastCount = 0
	var angle = 0.0
	while angle <= light.spot_angle * 2:
		var angle2 = 0
		while angle2 < 360:
			var direction = VectorExtensions.RotatedTo3D(forward, up, angle)
			direction = Quaternion(forward, angle2) * direction
			var contactPoint = Raycast(light.global_position, direction)
			if contactPoint != null:
				contactPoints.append(contactPoint)
			rayCastCount += 1
			angle2 += degreesPerRayCast
		angle += degreesPerRayCast
	return [contactPoints, rayCastCount]

func GetNoticability (light : SpotLight3D, degreesPerRayCast : float) -> float:
	var output = GetContactPoints(light, degreesPerRayCast)
	return GetNoticabilityFromContactPoints(light, output[0], output[1])

func GetNoticabilityFromContactPoints (light : SpotLight3D, contactPoints : Array[ContactPoint], rayCastCount : int) -> float:
	var output = 0.0
	for contactPoint in contactPoints:
		var distance = contactPoint.distance
		var lightColor = light.light_color
		output += light.light_energy * ColorExtensions.GetBrightness(lightColor) * (1.0 / (distance * distance)) * (1.0 - ColorExtensions.GetSimilarity(contactPoint.color, lightColor))
	output /= rayCastCount
	return output

class ContactPoint:
	var distance : float
	var color : Color

	static func New (distance : float, color : Color) -> ContactPoint:
		var output = new()
		output.distance = distance
		output.color = color
		return output

enum ColorMode
{
	Gradient,
	ColorPalette,
	RandomColor
}
