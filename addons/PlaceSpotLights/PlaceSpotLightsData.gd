extends Node
class_name PlaceSpotLightsData

@export var do : bool
@export var actOnScene : PackedScene
@export var targetPlaceCount : int
@export var placePointsParentName : String
@export var totalNoticabilityRange = FloatRange.New(0, PlaceSpotLights.BIG_NUMBER)
@export var eachLightNoticabilityRange = FloatRange.New(0, PlaceSpotLights.BIG_NUMBER)
@export var distanceFromContactPointRange = FloatRange.New(0, PlaceSpotLights.BIG_NUMBER)
@export var rangeMultiplierRange = FloatRange.New(0, PlaceSpotLights.BIG_NUMBER)
@export var intensityRange = FloatRange.New(0, PlaceSpotLights.BIG_NUMBER)
@export var spotAngleRange = FloatRange.New(0, 180)
@export var degreesPerRayCast : float
@export var colorMode : PlaceSpotLights.ColorMode
@export var validColorsGraidient : Gradient
@export var validColorPalette : ColorPalette
@export var maxRetries : int = 100
