class_name VectorExtensions

static func GetDeg2Rad (v : Vector2):
	return Vector2(deg_to_rad(v.x), deg_to_rad(v.y))

static func GetRad2Deg (v : Vector2):
	return Vector2(rad_to_deg(v.x), rad_to_deg(v.y))

static func Rotated90 (v : Vector2):
	return Vector2(-v.y, v.x)

static func Rotated270 (v : Vector2):
	return Vector2(v.y, -v.x)

static func Snapped (v : Vector2, interval : float):
	return Vector2(MathExtensions.SnapToInterval(v.x, interval), MathExtensions.SnapToInterval(v.y, interval))

static func GetFacingAngle (v : Vector2):
	v = v.normalized()
	return rad_to_deg(atan2(v.y, v.x))

static func FromFacingAngle (facingAngle : float):
	facingAngle = deg_to_rad(facingAngle)
	return Vector2(cos(facingAngle), sin(facingAngle)).normalized()

static func RotatedTo2D (from : Vector2, to : Vector2, angle : float):
	angle = deg_to_rad(angle)
	var fromAngle = deg_to_rad(VectorExtensions.GetFacingAngle(from))
	var toAngle = deg_to_rad(VectorExtensions.GetFacingAngle(to))
	var angleDifference = abs(angle_difference(fromAngle - angle, toAngle))
	var angleDifference2 = abs(angle_difference(fromAngle + angle, toAngle))
	var output
	if angleDifference > angleDifference2:
		if angleDifference2 <= angle:
			output = to.normalized() * from.length()
		else:
			output = Rotated(from, rad_to_deg(angle))
	else:
		if angleDifference <= angle:
			output = to.normalized() * from.length()
		else:
			output = Rotated(from, rad_to_deg(-angle))
	return output

static func RotatedTo3D (from : Vector3, to : Vector3, angle : float):
	var angleBetween = rad_to_deg(from.angle_to(to))
	if angleBetween <= angle:
		return to
	else:
		var rotateAround = from.cross(to)
		return from.rotated(rotateAround.normalized(), angle)

static func RotatedAroundPivot3D (v : Vector3, pivotPoint : Vector3, rotation : Quaternion):
	var direction = (rotation * (v - pivotPoint)).normalized()
	return pivotPoint + (direction * (v - pivotPoint).length())

static func Rotated (v : Vector2, angle : float):
	angle = GetFacingAngle(v) + angle
	angle = deg_to_rad(angle)
	return Vector2(cos(angle), sin(angle)).normalized() * v.length()

static func RotatedAroundPivot2D (v : Vector2, pivot : Vector2, angle : float):
	var ang = GetFacingAngle(v - pivot) + angle
	angle = deg_to_rad(angle)
	return pivot + (Vector2(cos(ang), sin(ang)).normalized() * (v - pivot).length())

static func Parse (string : String):
	var indexOfComma = string.find(",")
	return Vector2(float(string.substr(0, indexOfComma)), float(string.substr(indexOfComma + 1)))

static func ToVec3 (v : Vector2, z : float = 0):
	return Vector3(v.x, v.y, z)

static func ToVec3i (v : Vector3):
	return Vector3i(int(v.x), int(v.y), int(v.z))

static func Random2D (lengthRange = FloatRange.New(0, 1)):
	return FromFacingAngle(360 * randf()) * randf_range(lengthRange.min, lengthRange.max)

static func Random3D (lengthRange = FloatRange.New(0, 1)):
	return Quaternion.from_euler(Vector3(randf() * 360, randf() * 360, randf() * 360)).get_axis().normalized() * randf_range(lengthRange.min, lengthRange.max)

static func Multiply_float (v : Vector2, v2 : Vector2):
	return v.x * v2.x + v.y * v2.y

static func SetZ (v : Vector3, z : float):
	v.z = z
	return v

static func GetManhattenMagnitude (v : Vector2):
	return absf(v.x) + absf(v.y)

static func Min (v : Vector2, v2 : Vector2):
	return Vector2(minf(v.x, v2.x), minf(v.y, v2.y))

static func Max (v : Vector2, v2 : Vector2):
	return Vector2(maxf(v.x, v2.x), maxf(v.y, v2.y))

static func GetIndexOfClosestPoint (point : Vector2, points : Array[Vector2]):
	var closestPointIndex : int
	var closestDistanceSqr = (points[0] - point).length_squared()
	for i in range(1, points.size()):
		var point2 = points[i]
		var distanceSqr = (point2 - point).length_squared()
		if distanceSqr < closestDistanceSqr:
			closestDistanceSqr = distanceSqr
			closestPointIndex = i
	return closestPointIndex

static func GetXZ (v : Vector3):
	return Vector3(v.x, 0, v.z)

static func GetAverage (points : Array[Vector3]) -> Vector3:
	var output = points[0]
	for i in range(1, points.size()):
		var point = points[i]
		output += point
	output /= points.size()
	return output
