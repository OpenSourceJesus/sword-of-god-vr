class_name AABBExtensions

static func GetRandomPoint (aabb : AABB):
	return Vector3(randf_range(aabb.position.x, aabb.end.x), randf_range(aabb.position.y, aabb.end.y), randf_range(aabb.position.z, aabb.end.z))
