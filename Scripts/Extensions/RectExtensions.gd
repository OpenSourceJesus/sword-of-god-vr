class_name RectExtensions

static func GetRandomPoint (r : Rect2i):
	return Vector2(randf_range(r.position.x, r.end.x), randf_range(r.position.y, r.end.y))
