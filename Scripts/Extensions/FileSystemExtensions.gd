class_name FileSystemExtensions

static func GetAllFiles (folderPath : String):
	var output = GetFiles(folderPath)
	var foldersRemaining = GetFolders(folderPath)
	while foldersRemaining.size() > 0:
		var _folderPath = foldersRemaining[0]
		output.append_array(GetFiles(_folderPath))
		foldersRemaining.append_array(GetFolders(_folderPath))
		foldersRemaining.remove_at(0)
	return output

static func GetFolders (folderPath : String):
	var output : Array[String]
	for folder in DirAccess.get_directories_at(folderPath):
		output.append(folderPath + "/"  + folder)
	return output

static func GetFiles (folderPath : String):
	var output : Array[String]
	for file in DirAccess.get_files_at(folderPath):
		output.append(folderPath + "/" + file)
	return output
