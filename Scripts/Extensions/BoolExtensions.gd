class_name BoolExtensions

static func To01 (b : bool):
	if b:
		return 1
	else:
		return 0

static func ToSign (b : bool):
	if b:
		return 1
	else:
		return -1
