class_name NodeExtensions

static func GetNode (type, includeInternal = false, root : Node = null):
	if root == null:
		root = GameManager.instance.get_tree().current_scene
	var nodesToSearch : Array[Node]
	nodesToSearch.append(root)
	while nodesToSearch.size() > 0:
		var node = nodesToSearch[0]
		if is_instance_of(node, type):
			return node
		nodesToSearch.append_array(node.get_children(includeInternal))
		nodesToSearch.remove_at(0)
	return null

static func GetNodes (type, includeInternal = false, root : Node = null):
	if root == null:
		root = GameManager.instance.get_tree().current_scene
	var output : Array[Node]
	var nodesToSearch : Array[Node]
	nodesToSearch.append(root)
	while nodesToSearch.size() > 0:
		var node = nodesToSearch[0]
		if is_instance_of(node, type):
			output.append(node)
		nodesToSearch.append_array(node.get_children(includeInternal))
		nodesToSearch.remove_at(0)
	return output

static func GetClosestNode2D (closestTo : Node2D, type, includeInternal = false, root : Node2D = null):
	var nodes = GetNodes(type, includeInternal, root)
	nodes.erase(closestTo)
	if nodes.size() == 0:
		return null
	var closest = nodes[0]
	var closestDistanceSqr = (closest.global_position - closestTo.global_position).length_squared()
	for i in range(1, nodes.size()):
		var node = nodes[i]
		var distanceSqr = (node.global_position - closestTo.global_position).length_squared()
		if distanceSqr < closestDistanceSqr:
			closestDistanceSqr = distanceSqr
			closest = node
	return closest

static func GetClosestNode2DToPoint (position : Vector2, type, includeInternal = false, root : Node2D = null):
	var nodes = GetNodes(type, includeInternal, root)
	if nodes.size() == 0:
		return null
	var closest = nodes[0]
	var closestDistanceSqr = (closest.global_position - position).length_squared()
	for i in range(1, nodes.size()):
		var node = nodes[i]
		var distanceSqr = (node.global_position - position).length_squared()
		if distanceSqr < closestDistanceSqr:
			closestDistanceSqr = distanceSqr
			closest = node
	return closest

static func GetNode2DsWithinRange (closestTo : Node2D, range : float, type, includeInternal = false, root : Node2D = null):
	var output : Array[Node2D]
	var nodes = GetNodes(type, includeInternal, root)
	nodes.erase(closestTo)
	if nodes.size() == 0:
		return null
	for i in range(0, nodes.size()):
		var node = nodes[i]
		var distanceSqr = (node.global_position - closestTo.global_position).length_squared()
		if distanceSqr < range * range:
			output.append(node)
	return output

static func GetNode2DsWithinRangeToPoint (position : Vector2, range : float, type, includeInternal = false, root : Node2D = null):
	var output : Array[Node2D]
	var nodes = GetNodes(type, includeInternal, root)
	if nodes.size() == 0:
		return null
	for i in range(0, nodes.size()):
		var node = nodes[i]
		var distanceSqr = (node.global_position - position).length_squared()
		if distanceSqr < range * range:
			output.append(node)
	return output

static func GetClosestNode3DToPoint (position : Vector3, type, includeInternal = false, root : Node3D = null):
	var nodes = GetNodes(type, includeInternal, root)
	if nodes.size() == 0:
		return null
	var closest = nodes[0]
	var closestDistanceSqr = (closest.global_position - position).length_squared()
	for i in range(1, nodes.size()):
		var node = nodes[i]
		var distanceSqr = (node.global_position - position).length_squared()
		if distanceSqr < closestDistanceSqr:
			closestDistanceSqr = distanceSqr
			closest = node
	return closest

static func GetNode3DsWithinRange (closestTo : Node3D, range : float, type, includeInternal = false, root : Node3D = null):
	var output : Array[Node3D]
	var nodes = GetNodes(type, includeInternal, root)
	nodes.erase(closestTo)
	if nodes.size() == 0:
		return null
	for i in range(0, nodes.size()):
		var node = nodes[i]
		var distanceSqr = (node.global_position - closestTo.global_position).length_squared()
		if distanceSqr < range * range:
			output.append(node)
	return output
