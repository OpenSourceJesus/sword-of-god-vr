class_name ColorExtensions

static func SetAlpha (c : Color, a : float) -> Color:
	return Color(c.r, c.g, c.b, a)

static func AddAlpha (c : Color, a : float) -> Color:
	return SetAlpha(c, c.a + a)

static func MultiplyAlpha (c : Color, a : float) -> Color:
	return SetAlpha(c, c.a * a)

static func DivideAlpha (c : Color, a : float) -> Color:
	return SetAlpha(c, c.a / a)

static func Random () -> Color:
	return Color(randf(), randf(), randf())

static func GetAverage (colors : Array[Color]) -> Color:
	var output = colors[0]
	for i in range(1, colors.size()):
		var color = colors[i]
		output += color
	return output / colors.size()

static func GetLerpAverage (colors : Array[Color]) -> Color:
	var output = colors[0]
	for i in range(1, colors.size()):
		var color = colors[i]
		output = output.lerp(color, 1.0 / colors.size())
	return output

static func GetBrightness (c : Color) -> float:
	return (c.r + c.g + c.b) / 3

static func GetSimilarity (color : Color, otherColor : Color, testAlpha : bool = false) -> float:
	var rDifference = abs(color.r - otherColor.r)
	var gDifference = abs(color.g - otherColor.g)
	var bDifference = abs(color.b - otherColor.b)
	if testAlpha:
		var aDifference = abs(color.a - otherColor.a)
		return 1.0 - (rDifference + gDifference + bDifference + aDifference) / 4
	else:
		return 1.0 - (rDifference + gDifference + bDifference) / 3

static func GetOpposite (color : Color) -> Color:
	var output = color
	color.r = 1.0 - color.r
	color.g = 1.0 - color.g
	color.b = 1.0 - color.b
	color.a = 1.0 - color.a
	return output
