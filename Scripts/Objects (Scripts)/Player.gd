extends Destructable
class_name Player

@export var characterBody : CharacterBody3D
@export var xrCamera : XRCamera3D
@export var moveSpeed : float
@export var jumpSpeed : float
@export var lookSpeed : float
@export var attackSpeed : float
@export var damage : float
@export var maxAttackBounces : float
@export var attackRange : float
@export var hpImagesParent : Node
@export var maxHpImagesParent : Node
@export var itemsParent : Node
@export var freeRerolls : float
@export var haggling : float
@export var enemyCashMultiplier : float
@export var cash : float
@export var cashLabel : Label3D
@export var prayingTo : Array[God]
@export var distributePointsToGodsImPrayingTo : int
@export var deathMenu : Menu
@export var deathLabel : Label3D
@export var itemCooldownBar : TextureProgressBar
@export var itemCooldownLabel : Label3D
@export var radiationDamage : float
@export var radiationDistanceScale : float
@export var invulnerableIndicator : MeshInstance3D
@export var extraDamageIndicator : MeshInstance3D
@export var timeMultiplierWhileSpedUp : float
static var instance : Player
static var highscore : float
var multiplyDamage = 1.0
var maxCash = INF
var previousEnemyCashMultiplier : float
var deltaTime : float
var attacking : Enemy
var attackTimer : float
var previousHitEnemyCollider : Object
var previousHitItem : Item
var items : Array[Item]
var godEffectsDict : Dictionary
var godEffectsIHaveEnoughPointsFor : Array[GodEffect]
var score : float
var attackBouncesRemaining : int
var attackRangeRemaining : float
var attackingEnemies : Array[Enemy]
var invulnerable : bool
var movementVelocity : Vector3
var yVelocity : float
const SWORD_BEAM_SCENE = preload('res://Scenes/Objects (Scenes)/God Sword Beam.tscn')
const HP_IMAGE_SCENE = preload('res://Scenes/Concepts (Scenes)/UI Items (Scenes)/Hp Image.tscn')
const MAX_HP_IMAGE_SCENE = preload('res://Scenes/Concepts (Scenes)/UI Items (Scenes)/Max Hp Image.tscn')
const REPLACE_WITH_SCORE_INDICATOR = '_'
const REPLACE_WITH_HIGHSCORE_INDICATOR = '|'

func _ready ():
	super._ready ()
	instance = self
	SetHaggling (haggling)
	SetEnemyCashMultiplier (enemyCashMultiplier)
	for i in range(0, hp):
		var hpImage = HP_IMAGE_SCENE.instantiate()
		hpImagesParent.add_child(hpImage)
		var maxHpImage = MAX_HP_IMAGE_SCENE.instantiate()
		maxHpImagesParent.add_child(maxHpImage)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _process (deltaTime : float):
	if get_tree() == null || GameManager.paused:
		return
	self.deltaTime = deltaTime
	var enemies = NodeExtensions.GetNodes(Enemy)
	if enemies.size() > 0:
		var closestDistanceSqr = INF
		for enemy in enemies:
			if enemy.characterBody != null:
				var distanceSqr = (enemy.characterBody.global_position - characterBody.global_position).length_squared()
				if distanceSqr < closestDistanceSqr:
					closestDistanceSqr = distanceSqr
					attacking = enemy
	if attacking != null && attacking.targetedIndicator != null:
		attacking.targetedIndicator.visible = true
	HandleAttack ()
	if characterBody.is_on_floor():
		if Input.is_action_pressed('Jump'):
			yVelocity = jumpSpeed
		else:
			yVelocity = 0
	else:
		if yVelocity > 0 && !Input.is_action_pressed('Jump'):
			yVelocity = 0
		else:
			yVelocity -= ProjectSettings.get_setting('physics/3d/default_gravity') * deltaTime
	characterBody.velocity = characterBody.quaternion * movementVelocity * moveSpeed + Vector3.UP * yVelocity
	characterBody.move_and_slide()

func _input (inputEvent : InputEvent):
	if inputEvent is InputEventKey:
		movementVelocity = Vector3.ZERO
		if Input.is_action_pressed('Move Left'):
			movementVelocity.x += 1
		if Input.is_action_pressed('Move Right'):
			movementVelocity.x -= 1
		if Input.is_action_pressed('Move Back'):
			movementVelocity.z -= 1
		if Input.is_action_pressed('Move Forward'):
			movementVelocity.z += 1
		if movementVelocity != Vector3.ZERO:
			movementVelocity = movementVelocity.normalized()
		if Input.is_action_just_pressed('Reset'):
			%GameManager.Save ()
			get_tree().reload_current_scene()
		elif Input.is_action_just_pressed('Pause'):
			if PauseMenu.instance.open:
				PauseMenu.instance.Close ()
			else:
				PauseMenu.instance.Open ()
		elif Input.is_action_just_pressed('Map'):
			if %MapMenu.open:
				%MapMenu.Close ()
			else:
				%MapMenu.Open ()
		elif Input.is_action_just_pressed('Pray Menu'):
			if %PrayMenu.open:
				%PrayMenu.Close ()
			else:
				%PrayMenu.Open ()
		elif !GameManager.paused:
			if Input.is_action_just_pressed('Speed Up Time'):
				Engine.time_scale = timeMultiplierWhileSpedUp
			elif Input.is_action_just_released('Speed Up Time'):
				Engine.time_scale = 1
	elif inputEvent is InputEventMouseMotion:
		var look = -inputEvent.relative * lookSpeed
		characterBody.rotation_degrees += Vector3.UP * look.x
		xrCamera.rotation_degrees += Vector3.RIGHT * look.y

func HandleAttack ():
	if radiationDamage > 0:
		HandleRadiation ()
	if attacking == null && attackTimer <= 0:
		return
	if attackTimer > 0:
		attackTimer -= deltaTime
	if attackTimer <= 0:
		attackRangeRemaining = attackRange
		if attacking != null && attacking.characterBody != null:
			var toEnemy = attacking.characterBody.global_position - characterBody.global_position
			if toEnemy.length() <= attackRange:
				attackBouncesRemaining = maxAttackBounces
				attackingEnemies.clear()
				attackTimer += 1.0 / attackSpeed
				Attack (attacking, characterBody.global_position)

func HandleRadiation ():
	for enemy in Enemy.instances:
		var enemyDistance = (enemy.characterBody.global_position - characterBody.global_position).length()
		enemy.TakeDamage (clampf((radiationDistanceScale - enemyDistance) / radiationDistanceScale, 0, 1) * radiationDamage * multiplyDamage * deltaTime)

func Attack (enemy : Enemy, fromPosition : Vector3):
	var swordBeam = SWORD_BEAM_SCENE.instantiate()
	var toPosition = enemy.characterBody.global_position + Vector3.UP * enemy.height / 2
	Room.instance.add_child(swordBeam)
	swordBeam.global_position = (fromPosition + toPosition) / 2
	swordBeam.look_at(toPosition)
	swordBeam.scale = Vector3(1, 1, (fromPosition - toPosition).length())
	enemy.TakeDamage (damage * multiplyDamage)
	var blood = Enemy.BLOOD_SCENE.instantiate()
	Room.instance.add_child(blood)
	blood.global_position = toPosition
	blood.look_at(blood.global_position + (blood.global_position - fromPosition))
	blood.emitting = true
	attackRangeRemaining -= (toPosition - characterBody.global_position).length()
	attackingEnemies.append(enemy)
	if attackBouncesRemaining > 0 && attackRangeRemaining >= 0:
		var enemiesInRange = NodeExtensions.GetNode3DsWithinRange(enemy, attackRangeRemaining, Enemy)
		if enemiesInRange != null:
			for enemy2 in enemiesInRange:
				if attackBouncesRemaining > 0 && !attackingEnemies.has(enemy2):
					attackBouncesRemaining -= 1
					Attack (enemy2, toPosition)

func TakeDamage (amount : float):
	print(amount)
	if invulnerable:
		return
	var blockDamageEveryDamageIntervalGodEffect = NodeExtensions.GetNode(BlockDamageEveryDamageIntervalGodEffect, false, itemsParent)
	if blockDamageEveryDamageIntervalGodEffect != null:
		blockDamageEveryDamageIntervalGodEffect.damageTillBlock -= 1
		if blockDamageEveryDamageIntervalGodEffect.damageTillBlock == 0:
			blockDamageEveryDamageIntervalGodEffect.damageTillBlock += blockDamageEveryDamageIntervalGodEffect.interval
			return
	var previousHp = int(hp)
	super.TakeDamage (amount)
	var _hp = int(hp)
	if _hp > previousHp:
		for i in range(previousHp, _hp):
			var child = HP_IMAGE_SCENE.instantiate()
			hpImagesParent.add_child(child)
	else:
		for i in range(_hp, previousHp):
			var child = hpImagesParent.get_child(0)
			child.queue_free()

func UpdateMaxHpImages ():
	var maxHpImageCount = maxHpImagesParent.get_child_count()
	if maxHp > maxHpImageCount:
		for i in range(maxHpImageCount, maxHp):
			var maxHpImage = MAX_HP_IMAGE_SCENE.instantiate()
			maxHpImagesParent.add_child(maxHpImage)
	else:
		for i in range(maxHp, maxHpImageCount):
			var child = maxHpImagesParent.get_child(0)
			child.free()
			child = hpImagesParent.get_child(0)
			child.free()

func Die ():
	var reviveWithLessMaxHpGodEffect = NodeExtensions.GetNode(ReviveWithLessMaxHpGodEffect, false, itemsParent)
	if reviveWithLessMaxHpGodEffect != null:
		maxHp -= 1
		if maxHp > 0:
			return
	var reviveWithLessCashGodEffect = NodeExtensions.GetNode(ReviveWithLessCashGodEffect, false, itemsParent)
	if reviveWithLessCashGodEffect != null:
		AddCash (-reviveWithLessCashGodEffect.loseCashAmountOnRevive)
		maxCash = cash - reviveWithLessCashGodEffect.capDifferenceOnRevive
		if cash > 0:
			return
	dead = true
	deathMenu.Open ()
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	deathLabel.text = deathLabel.text.replace(REPLACE_WITH_SCORE_INDICATOR, str(int(score)))
	deathLabel.text = deathLabel.text.replace(REPLACE_WITH_HIGHSCORE_INDICATOR, str(int(highscore)))
	%GameManager.Save ()
	Room.instance.queue_free()
	queue_free()

func AddCash (amount : float):
	cash = clampf(cash + amount, 0, maxCash)
	cashLabel.text = str(int(cash))

func SetHaggling (amount : float):
	haggling = amount

func SetEnemyCashMultiplier (amount : float):
	previousEnemyCashMultiplier = enemyCashMultiplier
	enemyCashMultiplier = amount

func HasGodEffectOfSameTier (effect : GodEffect):
	for effects in godEffectsDict.values():
		for effect2 in effects:
			if effect2.get_parent() == effect.get_parent():
				return true
	return false
