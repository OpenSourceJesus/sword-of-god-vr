extends Node3D
class_name Bullet

@export var moveSpeed : float
@export var damage : int
@export var damageSpeed : int
@export var range : float
@export var duration : float
@export var despawnMode : DespawnMode
@export var rigidBody : RigidBody3D
@export var area : Area3D
static var instances : Array[Bullet]
var damaging : Array[Destructable]
var callOnUnpausedFrame = Callable()
var callOnDestroy = Callable()
var partOfBulletPattern : BulletPattern

func _ready ():
	instances.append(self)
	tree_exiting.connect(OnDestroy)
	if despawnMode == DespawnMode.None || rigidBody == null:
		set_physics_process(false)
	if rigidBody != null:
		rigidBody.body_entered.connect(OnEnterBody)
	if area != null:
		area.body_entered.connect(OnEnterArea)
		area.body_exited.connect(OnExitArea)

func _physics_process (deltaTime : float):
	if GameManager.paused:
		return
	if partOfBulletPattern != null && callOnUnpausedFrame != Callable():
		callOnUnpausedFrame.call(self)
	rigidBody.linear_velocity = (rigidBody.to_global(Vector3.FORWARD) - rigidBody.global_position).normalized() * moveSpeed
	for i in range(damaging.size()):
		var _damaging = damaging[i]
		if _damaging == null:
			damaging.remove_at(i)
			i -= 1
		else:
			_damaging.TakeDamage (damageSpeed * deltaTime)
	if despawnMode == DespawnMode.Range:
		range -= moveSpeed * deltaTime
		if range <= 0:
			OnDestroy ()
			queue_free()
	else:
		duration -= deltaTime
		if duration <= 0:
			OnDestroy ()
			queue_free()

func OnDestroy ():
	instances.erase(self)
	if callOnDestroy != Callable() && self != null:
		callOnDestroy.call_deferred(self)

func OnEnterArea (node : Node3D):
	damaging.append(node.get_parent())

func OnExitArea (node : Node3D):
	damaging.erase(node.get_parent())

func OnEnterBody (node : Node3D):
	if node.get_parent_node_3d() is Player:
		Player.instance.TakeDamage (damage)
	OnDestroy ()
	queue_free()

enum DespawnMode {
	None,
	Range,
	Time
}
