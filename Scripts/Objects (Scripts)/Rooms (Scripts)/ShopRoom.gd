extends Room
class_name ShopRoom

@export var rerollCostPerTotalCost : float
@export var maxCostVariationPerItem : int
@export var rerollButton : Button
@export var rerollButtonLabel : Label3D
@export var itemsParent : Node
var rerollCost
const ITEM_COUNT = 4

func _ready ():
	cleared = true
	Make ()

func Reroll ():
	Player.instance.AddCash (-rerollCost / (1.0 + Player.instance.haggling))
	for item in itemsParent.get_children():
		item.free()
	if Player.instance.freeRerolls > 0:
		Player.instance.freeRerolls -= 1
	Make ()

func Make ():
	Player.instance = get_tree().current_scene.find_child("Player")
	if Player.instance.freeRerolls > 0:
		rerollCost = 0
	else:
		rerollCost = (float(Game.instance.totalShopCost) * rerollCostPerTotalCost * Game.instance.multiplyShopCosts)
	rerollButtonLabel.text = "Reroll (" + str(MathExtensions.SnapToInterval(rerollCost / (1.0 + Player.instance.haggling), 0.1)) + ")"
	if rerollCost / (1.0 + Player.instance.haggling) > Player.instance.cash:
		rerollButton.disabled = true
	var remainingItemScenes = Game.ITEM_SCENES.duplicate()
	while true:
		var itemSceneIndex = randi_range(0, remainingItemScenes.size() - 1)
		var itemScene = remainingItemScenes[itemSceneIndex]
		var targetCost = float(Game.instance.totalShopCost) / ITEM_COUNT
		var costVariation = float(targetCost * maxCostVariationPerItem)
		var costRange = FloatRange.New(clampf(targetCost - costVariation, 0, targetCost), clampf(targetCost + costVariation, targetCost, INF))
		var item = itemScene.instantiate()
		var useableItem = item as UseableItem
		if useableItem != null:
			var indexOfSlash = itemScene.resource_path.rfind("/") + 1
			var indexOfPeriod = itemScene.resource_path.rfind(".")
			useableItem.id = itemScene.resource_path.substr(indexOfSlash, indexOfPeriod - indexOfSlash)
		item.cost *= Game.instance.multiplyShopCosts
		if costRange.Contains(item.cost / (1.0 + Player.instance.haggling)):
			var hasItemLimit = false
			if item.maxCount > 0:
				item.maxCount -= 1
			elif item.maxCount == 0:
				hasItemLimit = true
			if !hasItemLimit:
				var control = Control.new()
				item.inShop = true
				item.buyButtonLabel.text = "Buy (" + str(MathExtensions.SnapToInterval(item.cost, 0.1)) + ")"
				if item.cost / (1.0 + Player.instance.haggling) > Player.instance.cash:
					item.buyButton.disabled = true
				control.add_child(item)
				itemsParent.add_child(control)
				if itemsParent.get_child_count() == ITEM_COUNT:
					return
			else:
				item.free()
		else:
			item.free()
		remainingItemScenes.remove_at(itemSceneIndex)
		if remainingItemScenes.size() == 0:
			return
