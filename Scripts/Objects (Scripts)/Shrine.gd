extends Node3D
class_name Shrine

@export var god : God
@export var meshInstance : MeshInstance3D
@export var loseGodPointsForDestroying : int
@export var cashForDestroying : int
@export var addGodPointsPerHp : int
@export var loseGodPointsPerHp : int
@export var cashBatchSize : int
@export var addGodPointsPerCashBatch : int
@export var loseGodPointsPerCashBatch : int
@export var giveCashButton : Button
@export var infoLabel : Label3D
const REPLACE_WITH_GOD_NAME = "_"

func _ready ():
	giveCashButton.disabled = Player.instance.cash < cashBatchSize
	SetInfoLabel.call_deferred()

func SetInfoLabel ():
	infoLabel.text = infoLabel.text.replace(REPLACE_WITH_GOD_NAME, god.name)

func Destroy ():
	god.AddPoints (-loseGodPointsForDestroying)
	for god2 in Game.instance.gods:
		if god2 != god:
			god2.AddPoints (loseGodPointsForDestroying / (Game.instance.gods.size() - 1))
	Player.instance.AddCash (cashForDestroying)
	MapMenu.instance.roomIndicatorsDict[Room.instance.location].shrineIndicator.free()
	queue_free()

func GiveHp ():
	Player.instance.TakeDamage (1)
	god.AddPoints (addGodPointsPerHp)
	for god2 in Game.instance.gods:
		if god2 != god:
			god2.AddPoints (-loseGodPointsPerHp / (Game.instance.gods.size() - 1))

func GiveCash ():
	Player.instance.AddCash (-cashBatchSize)
	god.AddPoints (addGodPointsPerCashBatch)
	for god2 in Game.instance.gods:
		if god2 != god:
			god2.AddPoints (-loseGodPointsPerCashBatch / (Game.instance.gods.size() - 1))
	giveCashButton.disabled = Player.instance.cash < cashBatchSize
