extends Collectable
class_name Item

@export var cost : float
@export var maxCount : int = -1
@export var combinedWith : Array[Item]
@export var buyButton : Button
@export var buyButtonLabel : Label3D
@export var descriptionLabel : Label3D
@export var collider : CollisionShape3D
var inShop : bool

func _ready ():
	if !Room.instance.GetAABB().has_point(global_position):
		global_position *= -1

func Collect ():
	if inShop:
		Player.instance.AddCash (-cost / (1.0 + Player.instance.haggling))
		var shopRoom = Room.instance as ShopRoom
		for itemContainer in shopRoom.itemsParent.get_children():
			var item = itemContainer.get_child(0)
			if item != null:
				item.buyButton.disabled = Player.instance.cash < cost
	for item in combinedWith:
		item.Collect ()
	if !(get_parent() is Item):
		reparent(Player.instance.itemsParent)
		position = Vector3.ZERO
		buyButton.get_parent().visible = false
		collider.disabled = true
		Player.instance.items.append(self)
	visible = false
