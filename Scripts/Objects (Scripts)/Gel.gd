extends Node3D
class_name Gel

@export var sourceNode : Node3D
@export var makeRadius : float
@export var makeDistance : float
@export var duration : float
@export var collisionPolygon : CollisionPolygon3D
@export var line : Line2D
var previousSourcePosition : Vector3
var makePointTimes : PackedFloat32Array
var time : float

func _ready ():
	Reparent.call_deferred()
	previousSourcePosition = sourceNode.global_position
	line.add_point(previousSourcePosition)
	makePointTimes.append(0)

func Reparent ():
	reparent(Room.instance)
	global_position = Vector3.ZERO
	global_rotation_degrees = 0
	global_scale = Vector3.ONE

func _physics_process (deltaTime : float):
	if sourceNode != null && (sourceNode.global_position - previousSourcePosition).length_squared() > makeDistance * makeDistance:
		previousSourcePosition = sourceNode.global_position
		line.add_point(previousSourcePosition)
		makePointTimes.append(time)
	time += deltaTime
	if makePointTimes.size() > 0:
		while time >= makePointTimes[0] + duration:
			makePointTimes.remove_at(0)
			if makePointTimes.size() == 0:
				free()
				return
			line.remove_point(0)
