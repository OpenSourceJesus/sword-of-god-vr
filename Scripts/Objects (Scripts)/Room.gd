extends Node3D
class_name Room

@export var size : Vector3i
@export var spawnBorder : int
@export var portalsParent : Node3D
static var instance : Room
var location : Vector3i
var cleared : bool
var difficulty : float

func GetAABB ():
	return AABB(-size / 2, size)

func Clear ():
	cleared = true
	Game.instance.roomsTillShopRoom -= 1
	Game.instance.roomsTillShrineRoom -= 1
	Game.instance.totalShopCost += Game.instance.addToTotalShopCost
	Game.instance.multiplyShopCosts += Game.instance.addToMultiplyShopCosts
	Game.instance.multiplyDifficultyTillHeart += Game.instance.addToMultiplyDifficultyTillHeart
	GodEffectMenu.godsForTiersThatWentUp.clear()
	GodEffectMenu.godsForTiersThatWentDown.clear()
	for item in Player.instance.items:
		var useableItem = item as UseableItem
		if useableItem != null && useableItem.cooldownType == UseableItem.CooldownType.RoomsCleared:
			useableItem.SetCooldown (useableItem.cooldownRemaining - 1)
	for godEffects in Player.instance.godEffectsDict.values():
		for godEffect in godEffects:
			var addToHpAfterRoomRangeGodEffect = godEffect as AddToHpAfterRoomRangeGodEffect
			if addToHpAfterRoomRangeGodEffect != null:
				addToHpAfterRoomRangeGodEffect.roomsRemaining -= 1
				if addToHpAfterRoomRangeGodEffect.roomsRemaining == 0:
					Player.instance.TakeDamage (addToHpAfterRoomRangeGodEffect.amount)
					addToHpAfterRoomRangeGodEffect.roomsRemaining = randi_range(addToHpAfterRoomRangeGodEffect.roomRange.min, addToHpAfterRoomRangeGodEffect.roomRange.max)
			else:
				var addToCashEachRoomGodEffect = godEffect as AddToCashEachRoomGodEffect
				if addToCashEachRoomGodEffect != null:
					Player.instance.AddCash (addToCashEachRoomGodEffect.amount)
				else:
					var multiplyCashEachRoomGodEffect = godEffect as MultiplyCashEachRoomGodEffect
					if multiplyCashEachRoomGodEffect != null:
						Player.instance.AddCash (Player.instance.cash * multiplyCashEachRoomGodEffect.amount - Player.instance.cash)
					else:
						var addToDamageMultiplierEachRoomGodEffect = godEffect as AddToDamageMultiplierEachRoomGodEffect
						if addToDamageMultiplierEachRoomGodEffect != null:
							Player.instance.multiplyDamage += addToDamageMultiplierEachRoomGodEffect.amount
	for god in Player.instance.prayingTo:
		god.AddPoints (Player.instance.distributePointsToGodsImPrayingTo / Player.instance.prayingTo.size())
	portalsParent.visible = true
	var portals : Array[Portal]
	portals.append_array(portalsParent.get_children())
	while len(portals) > 0:
		var portal = portals.pick_random()
		portals.erase(portal)
		var newRoomOffset = portal.position
		newRoomOffset.y = 0
		newRoomOffset = newRoomOffset.normalized()
		newRoomOffset = VectorExtensions.ToVec3i(newRoomOffset)
		var room = Game.instance.NewRoom(location + newRoomOffset)
		room.visible = false
		if room is ShrineRoom:
			portal.roomDescriptionLabel.text = 'Shrine Room'
		elif room is ShopRoom:
			portal.roomDescriptionLabel.text = 'Shop Room'
		else:
			portal.roomDescriptionLabel.text = 'Difficulty:\n' + str(room.difficulty)
