extends Item
class_name Heart

static var heartCountInCurrentRoom : int

func _ready ():
	super._ready ()
	if !inShop:
		heartCountInCurrentRoom += 1
		#MapMenu.instance.roomIndicatorsDict[Room.instance.location].hasHeartIndicator.visible = true

func Collect ():
	super.Collect ()
	#if !inShop:
		#heartCountInCurrentRoom -= 1
		#if heartCountInCurrentRoom == 0:
			#MapMenu.instance.roomIndicatorsDict[Room.instance.location].hasHeartIndicator.visible = false
	Player.instance.items.erase(self)
	queue_free()
