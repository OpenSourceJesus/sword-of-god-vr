extends UseableItem
class_name AlienFriend

@export var cashAmountPerHeart : float

func _ready ():
	super._ready ()

func Use ():
	var heart = NodeExtensions.GetNode(Heart, false, Room.instance)
	if heart != null && !heart.inShop:
		heart.queue_free()
		Player.instance.AddCash (cashAmountPerHeart)
		super.Use ()
