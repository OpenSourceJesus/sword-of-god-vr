extends UseableItem
class_name EnergySphere

@export var bulletScenePath : String
var bulletScene : PackedScene

func _ready ():
	super._ready ()
	bulletScene = load(bulletScenePath)

func Use ():
	super.Use ()
	var bullet = bulletScene.instantiate()
	bullet.rigidBody.global_position = Player.instance.global_position
	var mousePosition = get_viewport().get_mouse_position()
	bullet.rigidBody.look_at(mousePosition)
	bullet.moveSpeed = (Player.instance.global_position - mousePosition).length() / bullet.duration
	get_tree().current_scene.add_child(bullet)
