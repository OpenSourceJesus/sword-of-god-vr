extends Item
class_name UseableItem

@export var maxUseDuration : float
@export var cooldown : float
@export var cooldownType : CooldownType
static var equipped : UseableItem
static var itemsDict : Dictionary
static var canUseItemsDict : Dictionary
var using : bool
var cooldownRemaining : float
var id : String

func _ready ():
	super._ready ()
	set_process_input(false)
	set_process(false)

func Collect ():
	super.Collect ()
	if !canUseItemsDict.has(id):
		canUseItemsDict[id] = []
		itemsDict[id] = []
		set_process_input(true)
	itemsDict[id].append(self)
	set_process(cooldownType == CooldownType.Time)

func _process (deltaTime : float):
	SetCooldown (cooldownRemaining - deltaTime)

func _input (event : InputEvent):
	if event is InputEventMouseButton:
		if Input.is_action_just_pressed("Item"):
			if maxUseDuration > 0:
				StartUsing ()
			else:
				Use ()
		elif using && Input.is_action_just_released("Item"):
			EndUsing ()

func Use ():
	SetCooldown (cooldown)

func StartUsing ():
	using = true

func EndUsing ():
	using = false
	SetCooldown (cooldown)

func SetCooldown (amount : float):
	cooldownRemaining = amount
	for item in itemsDict[id]:
		if item.cooldownRemaining <= 0:
			if canUseItemsDict[id].size() == 0:
				canUseItemsDict[id].append(item)
				item.set_process_input(true)
		else:
			canUseItemsDict[id].erase(item)
			item.set_process_input(false)
	if Player.instance == null:
		return
	Player.instance.itemCooldownBar.value = cooldownRemaining / cooldown
	if cooldownRemaining >= 1:
		Player.instance.itemCooldownLabel.text = str(int(cooldownRemaining))
	else:
		Player.instance.itemCooldownLabel.text = str(MathExtensions.SnapToInterval(cooldownRemaining, 0.1))
	Player.instance.itemCooldownBar.visible = cooldownRemaining > 0

enum CooldownType {
	Time,
	RoomsCleared
}
