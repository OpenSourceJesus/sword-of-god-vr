extends Item
class_name AddToHpItem

@export var amount : float

func Collect ():
	super.Collect ()
	Player.instance.TakeDamage (-amount)
