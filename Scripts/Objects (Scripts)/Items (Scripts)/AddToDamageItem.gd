extends Item
class_name AddToDamageItem

@export var amount : float

func Collect ():
	super.Collect ()
	Player.instance.damage += amount
