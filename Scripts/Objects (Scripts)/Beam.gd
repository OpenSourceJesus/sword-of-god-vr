extends Node
class_name Beam

@export var duration : float
@export var meshInstance : MeshInstance3D
var timeRemaining : float

func _ready ():
	timeRemaining = duration

func _process (deltaTime : float):
	timeRemaining = clampf(timeRemaining - deltaTime, 0, duration)
	var shaderMaterial = meshInstance.get_active_material(0) as ShaderMaterial
	if shaderMaterial != null:
		var tint : Color = shaderMaterial.get_shader_parameter('tint')
		tint.a = timeRemaining / duration
		shaderMaterial.set_shader_parameter('tint', tint)
		meshInstance.set_surface_override_material(0, shaderMaterial)
	if timeRemaining <= 0:
		free()
