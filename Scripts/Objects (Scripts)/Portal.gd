extends Node3D
class_name Portal

@export var area : Area3D
@export var roomDescriptionLabel : Label3D

func _ready ():
	area.body_entered.connect(OnBodyEnter)

func OnBodyEnter (node : Node3D):
	if is_visible_in_tree():
		Room.instance.visible = false
		var exitDirection = Room.instance.global_position - global_position
		exitDirection.y = 0
		exitDirection = VectorExtensions.ToVec3i(exitDirection.normalized())
		Game.instance.ExitRoom (exitDirection)
