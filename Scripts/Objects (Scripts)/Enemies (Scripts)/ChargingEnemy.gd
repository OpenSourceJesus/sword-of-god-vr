extends Enemy
class_name ChargingEnemy

@export var speedMultiplierWhenCharging : float
@export var chargeRange : float
@export var chargeReloadTime : float
@export var damage : int
var chargeDirection : Vector3
var charging : bool
var chargeRangeRemaining : float
var chargeReloadTimeRemaining : float
var wasPreviouslyColliding : bool

func HandleMove ():
	var move : Vector3
	var toPlayer = Player.instance.characterBody.global_position - characterBody.global_position
	if characterBody.is_on_floor():
		yVelocity = 0
	else:
		yVelocity += -ProjectSettings.get_setting('physics/3d/default_gravity') * deltaTime
	if !charging:
		move = toPlayer.normalized() * moveSpeed
		chargeReloadTimeRemaining -= deltaTime
		if chargeReloadTimeRemaining <= 0 && toPlayer.length_squared() <= chargeRange * chargeRange:
			chargeRangeRemaining = chargeRange
			moveSpeed *= speedMultiplierWhenCharging
			chargeDirection = move.normalized()
			charging = true
	else:
		move = chargeDirection * moveSpeed
		chargeRangeRemaining -= moveSpeed * deltaTime
		if chargeRangeRemaining <= 0:
			chargeReloadTimeRemaining = chargeReloadTime
			moveSpeed /= speedMultiplierWhenCharging
			charging = false
	move.y = 0
	characterBody.look_at(characterBody.global_position + Vector3(move.z, 0, -move.x))
	characterBody.velocity = move + Vector3.UP * yVelocity
	characterBody.move_and_slide()
	var collision = characterBody.get_last_slide_collision()
	if collision != null:
		for i in range(collision.get_collision_count()):
			var collider = collision.get_collider()
			if collider.get_parent() == Player.instance:
				if !wasPreviouslyColliding && charging:
					Player.instance.TakeDamage (damage)
				wasPreviouslyColliding = true
				return
	wasPreviouslyColliding = false
