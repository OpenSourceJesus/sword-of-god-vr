extends Destructable
class_name Enemy

@export var moveSpeed : float
@export var reloadSpeed : float
@export var followDistance : float
@export var bulletPatternEntry : BulletPatternEntry
@export var characterBody : CharacterBody3D
@export var difficulty : float
@export var targetedIndicator : CanvasItem
@export var height : float
static var instances : Array[Enemy]
var deltaTime : float
var shootTimer : float
var yVelocity : float
const BLOOD_SCENE = preload('res://Scenes/Objects (Scenes)/Blood.tscn')

func _ready ():
	super._ready ()
	shootTimer = reloadSpeed * randf()
	instances.append(self)
	characterBody = find_child('CharacterBody3D')
	bulletPatternEntry = find_child('Bullet Pattern Entry')

func _exit_tree ():
	instances.erase(self)

func _process (deltaTime : float):
	if GameManager.paused:
		return 
	self.deltaTime = deltaTime
	HandleMove ()
	HandleAttack ()

func HandleMove ():
	if characterBody == null:
		return
	var toPlayer = Player.instance.characterBody.global_position - characterBody.global_position
	var move : Vector3
	if toPlayer.length_squared() > followDistance * followDistance:
		move = toPlayer.normalized() * moveSpeed
	else:
		move = -toPlayer.normalized() * moveSpeed
	move.y = 0
	if characterBody.is_on_floor():
		yVelocity = 0
	else:
		yVelocity += -ProjectSettings.get_setting('physics/3d/default_gravity') * deltaTime
	characterBody.velocity = move + Vector3.UP * yVelocity
	characterBody.global_rotation_degrees = Vector3.UP * VectorExtensions.GetFacingAngle(Vector2(-toPlayer.x, toPlayer.z))
	characterBody.move_and_slide()

func HandleAttack ():
	if reloadSpeed == -1 || bulletPatternEntry == null:
		return
	shootTimer -= deltaTime
	if shootTimer <= 0:
		shootTimer += reloadSpeed
		bulletPatternEntry.Shoot ()

func Die ():
	super.Die()
	Game.instance.difficultyTillHeart -= difficulty
	if Game.instance.difficultyTillHeart <= 0:
		var heart = Game.HEART_SCENE.instantiate()
		Room.instance.add_child(heart)
		heart.global_position = characterBody.global_position
		Game.instance.difficultyTillHeart = randf_range(Game.instance.difficultyPerHeartRange.min, Game.instance.difficultyPerHeartRange.max) * Game.instance.multiplyDifficultyTillHeart
	Player.instance.score += difficulty
	if Player.instance.score > Player.instance.highscore:
		Player.instance.highscore = Player.instance.score
	Player.instance.AddCash (difficulty * Game.instance.cashPerEnemyDifficulty * Player.instance.enemyCashMultiplier)
	instances.erase(self)
	if instances.size() == 0:
		Room.instance.Clear ()
