extends Menu
class_name SettingsMenu

static var instance : SettingsMenu

func _ready ():
	super._ready ()
	instance = self

func Open ():
	super.Open ()
	#global_position = _Camera2D.instance.global_position - size / 2
	#GameManager.instance.closeHelpScreenButton.disabled = true

func OpenPauseMenu ():
	Close ()
	PauseMenu.instance.Open ()

static func ClearData ():
	GameManager.config.clear()
	GameManager.config.save(GameManager.SAVE_FILE_PATH)
	GameManager.initialized = false
	GameManager.SetScene ("res://Scenes/Game.tscn")
