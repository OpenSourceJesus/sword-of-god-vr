extends Menu
class_name PrayMenu

@export var buttonLabels : Array[Label]
@export var pointsLabels : Array[Label]

func Open ():
	super.Open ()
	for i in range(pointsLabels.size()):
		var pointsLabel = pointsLabels[i]
		pointsLabel.text = str(Game.instance.gods[i].points)

func TogglePrayToGod (godIndex : int):
	var god = Game.instance.gods[godIndex]
	if !Player.instance.prayingTo.has(god):
		Player.instance.prayingTo.append(god)
		buttonLabels[godIndex].text = god.name + "\n(Stop praying to)"
	else:
		Player.instance.prayingTo.erase(god)
		buttonLabels[godIndex].text = god.name + "\n(Start praying to)"
