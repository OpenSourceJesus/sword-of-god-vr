extends Menu
class_name MapMenu

@export var roomIndicatorsParent : Node3D
@export var roomIndicatorsDict : Dictionary
@export var roomIndicatorsSeparation : float
@export var playerIndicator : Node3D
static var instance : MapMenu

func _ready ():
	super._ready ()
	instance = self
