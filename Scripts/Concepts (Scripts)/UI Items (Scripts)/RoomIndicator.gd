extends UIItem
class_name RoomIndicator

@export var room : Room
@export var hasHeartIndicator : CanvasItem
