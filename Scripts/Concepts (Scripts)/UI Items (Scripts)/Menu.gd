extends UIItem
class_name Menu

@export var open : bool
@export var pauseOnOpen : bool
@export var unpauseOnClose : bool

func _ready ():
	if open:
		Open ()
	else:
		Close ()

func Open ():
	open = true
	visible = true
	if pauseOnOpen:
		GameManager.paused = true
		var rigidBodies = NodeExtensions.GetNodes(RigidBody2D)
		for rigidBody in rigidBodies:
			rigidBody.linear_velocity = Vector2.ZERO
			rigidBody.angular_velocity = 0
		Engine.time_scale = 0
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func Close ():
	open = false
	visible = false
	if unpauseOnClose:
		GameManager.paused = false
		Engine.time_scale = 1
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
