@tool
extends VisualShaderNodeCustom
class_name Spherize

func _get_name ():
	return "Spherize"
	
func _get_input_port_name (port : int):
	match port:
		0:
			return "uv"
		1:
			return "center"
		2:
			return "strength"
		3:
			return "offset"

func _get_global_code (mode : Shader.Mode):
	return """
		vec2 Spherize (vec2 uv, vec2 center, vec2 strength, vec2 offset) {
			vec2 delta = uv - center;
			float delta2 = dot(delta, delta);
			float delta4 = delta2 * delta2;
			vec2 delta_offset = strength * delta4;
			return uv + delta * delta_offset + offset;
		}
	"""

func _get_code (inputs : Array[String], outputs : Array[String], mode : Shader.Mode, type : VisualShader.Type):
	return outputs[0] + " = Spherize(%s, %s, %s, %s);" % [ inputs[0], inputs[1], inputs[2], inputs[3] ]

func _get_input_port_count ():
	return 4

func _get_input_port_default_value (port : int):
	match port:
		0:
			return Vector2.ZERO
		1:
			return Vector2.ONE / 2
		2:
			return Vector2.ONE * 10
		3:
			return Vector2.ZERO

func _get_input_port_type (port : int):
	return PORT_TYPE_VECTOR_2D

func _get_output_port_type (port : int):
	return PORT_TYPE_VECTOR_2D

func _get_output_port_count ():
	return 1
