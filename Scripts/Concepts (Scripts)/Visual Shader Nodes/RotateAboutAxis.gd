@tool
extends VisualShaderNodeCustom
class_name RotateAboutAxis

func _get_name ():
	return "RotateAboutAxis"
	
func _get_input_port_name (port : int):
	match port:
		0:
			return "input"
		1:
			return "axis"
		2:
			return "degrees"

func _get_global_code (mode : Shader.Mode):
	return """
		vec3 RotateAboutAxis (vec3 In, vec3 Axis, float Rotation) {
			Rotation = radians(Rotation);
			float s = sin(Rotation);
			float c = cos(Rotation);
			float one_minus_c = 1.0 - c;
			Axis = normalize(Axis);
			mat3 rotationMatrix = mat3(
				vec3(one_minus_c * Axis.x * Axis.x + c, one_minus_c * Axis.x * Axis.y - Axis.z * s, one_minus_c * Axis.z * Axis.x + Axis.y * s),
				vec3(one_minus_c * Axis.x * Axis.y + Axis.z * s, one_minus_c * Axis.y * Axis.y + c, one_minus_c * Axis.y * Axis.z - Axis.x * s),
				vec3(one_minus_c * Axis.z * Axis.x - Axis.y * s, one_minus_c * Axis.y * Axis.z + Axis.x * s, one_minus_c * Axis.z * Axis.z + c)
			);
			rotationMatrix = transpose(rotationMatrix);
			return rotationMatrix * In;
		}
	"""

func _get_code (inputs : Array[String], outputs : Array[String], mode : Shader.Mode, type : VisualShader.Type):
	return outputs[0] + " = RotateAboutAxis (%s, %s, %s);" % [ inputs[0], inputs[1], inputs[2] ]

func _get_input_port_count ():
	return 3

func _get_input_port_default_value (port : int):
	match port:
		0:
			return Vector3.ZERO
		1:
			return Vector3.ZERO
		2:
			return 0

func _get_input_port_type (port : int):
	match port:
		0:
			return PORT_TYPE_VECTOR_3D
		1:
			return PORT_TYPE_VECTOR_3D
		2:
			return PORT_TYPE_SCALAR

func _get_output_port_type (port : int):
	return PORT_TYPE_VECTOR_3D

func _get_output_port_count ():
	return 1
