extends Camera2D
class_name _Camera2D

@export var targetZoom : Vector2
@export var targetWindowSize : Vector2
static var instance : _Camera2D

func _ready ():
	if instance == null:
		instance = self
	get_viewport().size_changed.connect(OnWindowResize)
	OnWindowResize ()

func OnWindowResize ():
	var renderingDevice = RenderingServer.get_rendering_device()
	var windowSize = Vector2(renderingDevice.screen_get_width(), renderingDevice.screen_get_height())
	zoom = targetZoom / max(targetWindowSize.x / windowSize.x, targetWindowSize.y / windowSize.y)
