extends Node3D
class_name Interactable

@export var area : Area3D
@export var key : Key
@export var pressButtonOnInteract : Button
@export var pressKeyToInteractLabel : Label3D
var previousPressed : bool

func _ready ():
	area.body_entered.connect(OnBodyEnter)
	area.body_exited.connect(OnBodyExit)

func Interact ():
	if !pressButtonOnInteract.disabled:
		pressButtonOnInteract.pressed.emit()

func OnBodyEnter (node : Node3D):
	set_process_input(true)
	pressKeyToInteractLabel.visible = true
	set_process(true)

func OnBodyExit (node : Node3D):
	set_process_input(false)
	pressKeyToInteractLabel.visible = false
	set_process(false)

func _input (inputEvent : InputEvent):
	if inputEvent is InputEventKey:
		if inputEvent.keycode == key && inputEvent.pressed && !previousPressed:
			Interact ()
		previousPressed = inputEvent.pressed

func _process (deltaTime : float):
	pressKeyToInteractLabel.visible = !pressButtonOnInteract.disabled
