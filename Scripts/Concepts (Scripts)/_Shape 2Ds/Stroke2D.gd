extends _Shape2D
class_name Stroke2D

@export var startPoint : Vector2
@export var startDirection : Vector2
@export var controlPoints : Array
@export var turnRateCurve :  Curve
@export var widthCurve : Curve
@export var length : float
@export var sampleIntervals : PackedFloat32Array

static func New (startPoint : Vector2, startDirection : Vector2, controlPoints : Array, turnRateCurve : Curve, widthCurve : Curve, length : float, sampleIntervals : PackedFloat32Array):
	var output = new()
	output.startPoint = startPoint
	output.startDirection = startDirection
	output.controlPoints = controlPoints
	output.turnRateCurve = turnRateCurve
	output.widthCurve = widthCurve
	output.length = length
	output.sampleIntervals = sampleIntervals
	output.SetCornersOfPolygon ()
	output.SetEdgesOfPolygon ()
	return output

func SetCornersOfPolygon ():
	var lengthTraveled = 0.0
	var normalizedLengthTraveled = lengthTraveled / length
	var firstCorners : PackedVector2Array
	var secondCorners : PackedVector2Array
	var forward = startDirection.normalized()
	var position = startPoint
	var controlPointIndex = 0
	var controlPoint = controlPoints[0]
	var nextControlPoint : ControlPoint = null
	if controlPoints.size() > 1:
		nextControlPoint = controlPoints[1]
	var positionIndex = 0
	while true:
		var radius = widthCurve.sample(normalizedLengthTraveled) / 2
		var turnRate = turnRateCurve.sample(normalizedLengthTraveled)
		var tangent = VectorExtensions.Rotated90(forward)
		firstCorners.append(position - tangent * radius)
		secondCorners.insert(0, position + tangent * radius)
		var sampleInterval = sampleIntervals[positionIndex]
		forward = VectorExtensions.RotatedTo2D(forward, controlPoint.point - position, turnRate * sampleInterval)
		lengthTraveled += sampleInterval
		normalizedLengthTraveled = lengthTraveled / length
		if nextControlPoint != null && lengthTraveled >= nextControlPoint.startUsingAtLength:
			controlPoint = nextControlPoint
			controlPointIndex += 1
			if controlPoints.size() > controlPointIndex:
				nextControlPoint = controlPoints[controlPointIndex]
		if lengthTraveled >= length:
			break
		position += forward * sampleInterval
		if positionIndex < sampleIntervals.size() - 1:
			positionIndex += 1
	firstCorners.append_array(secondCorners)
	corners = firstCorners

func Moved (movement : Vector2):
	var output = new()
	for corner in corners:
		output.corners.append(corner + movement)
	output.SetEdgesOfPolygon ()
	output.startPoint += movement
	output.widthCurve = widthCurve
	output.turnRateCurve = turnRateCurve
	output.length = length
	output.sampleIntervals = sampleIntervals
	for controlPoint in controlPoints:
		controlPoint.point += movement
		output.controlPoints.append(controlPoint)
	return output

#func Rotated (pivotPoint : Vector2, degrees : float):
	#Stroke2D output = new Stroke2D()
	#output.corners = new Vector2[corners.Length]
	#for (int i = 0 i < corners.Length i ++)
		#output.corners[i] = corners[i].Rotate(pivotPoint, degrees)
	#output.SetEdgesOfPolygon ()
	#output.startPoint = startPoint.Rotate(pivotPoint, degrees)
	#output.startDirection = startDirection.Rotate(degrees)
	#output.widthCurve = widthCurve
	#output.turnRateCurve = turnRateCurve
	#output.length = length
	#output.sampleIntervals = sampleIntervals
	#output.controlPoints = new ControlPoint[controlPoints.Length]
	#for (int i = 0 i < controlPoints.Length i ++)
	#{
		#ControlPoint controlPoint = controlPoints[i]
		#controlPoint.point = controlPoint.point.Rotate(pivotPoint, degrees)
		#output.controlPoints[i] = controlPoint
	#}
	#return output
#}

#Stroke2D Rotated (float degrees)
#{
	#Stroke2D output = new Stroke2D()
	#output.corners = new Vector2[corners.Length]
	#for (int i = 0 i < corners.Length i ++)
		#output.corners[i] = corners[i].Rotate(degrees)
	#output.SetEdgesOfPolygon ()
	#output.startPoint = startPoint.Rotate(degrees)
	#output.startDirection = startDirection.Rotate(degrees)
	#output.widthCurve = widthCurve
	#output.turnRateCurve = turnRateCurve
	#output.length = length
	#output.sampleIntervals = sampleIntervals
	#output.controlPoints = new ControlPoint[controlPoints.Length]
	#for (int i = 0 i < controlPoints.Length i ++)
	#{
		#ControlPoint controlPoint = controlPoints[i]
		#controlPoint.point = controlPoint.point.Rotate(degrees)
		#output.controlPoints[i] = controlPoint
	#}
	#return output
#}

static func Straight (startPoint : Vector2, endPoint : Vector2, width : float):
	var widthCurve = Curve.new()
	widthCurve.add_point(Vector2(0, width))
	var toEndPoint = endPoint - startPoint
	var length = toEndPoint.magnitude
	return New(startPoint, toEndPoint, [ ControlPoint.new() ], Curve.new(), widthCurve, length, [ length ])

static func FromPoints (widthCurve : Curve, points : PackedVector2Array):
	var sampleIntervals : PackedFloat32Array
	var controlPoints : Array[ControlPoint]
	var length = 0.0
	var turnRateCurve = Curve.new()
	turnRateCurve.add_point(Vector2(0, 99999))
	var startPoint = points[0]
	var previousPoint = startPoint
	for i in range(1, points.size()):
		var point = points[i]
		var toPoint = point - previousPoint
		sampleIntervals.insert(i - 1, toPoint.length())
		controlPoints.insert(i - 1, ControlPoint.New(point, length))
		length += toPoint.length()
		previousPoint = point
	return New(startPoint, points[1] - startPoint, controlPoints, turnRateCurve, widthCurve, length, sampleIntervals)

static func FromPointsWithSampleInterval (widthCurve : Curve, sampleInterval : float, points : PackedVector2Array):
	var controlPoints : Array[ControlPoint]
	var length = 0.0
	var turnRateCurve = Curve.new()
	turnRateCurve.add_point(Vector2(0, 99999))
	var startPoint = points[0]
	var previousPoint = startPoint
	for i in range(1, points.size()):
		var point = points[i]
		var toPoint = point - previousPoint
		controlPoints.insert(i - 1, ControlPoint.New(point, length))
		length += toPoint.length()
		previousPoint = point
	return New(startPoint, points[1] - startPoint, controlPoints, turnRateCurve, widthCurve, length, [ sampleInterval ])

class ControlPoint:
	@export var point : Vector2
	@export var startUsingAtLength : float

	static func New (point : Vector2, startUsingAtLength : float):
		var output = ControlPoint.new()
		output.point = point
		output.startUsingAtLength = startUsingAtLength
		return output
