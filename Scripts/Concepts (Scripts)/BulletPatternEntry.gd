extends Node
class_name BulletPatternEntry

@export var bulletPattern : BulletPattern
@export var bulletScenePath : String
@export var spawnNode : Node3D

func _ready ():
	bulletPattern.Init (spawnNode)

func Shoot ():
	return bulletPattern.Shoot(bulletScenePath, spawnNode)
