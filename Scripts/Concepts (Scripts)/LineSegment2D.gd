@tool
#extends EditorPlugin
class_name LineSegment2D

@export var start : Vector2
@export var end : Vector2
#@export var drawer : CanvasItem
@export var drawColor : Color
@export var drawWidth : float = -1
#const SCENE = preload("res://Scenes/Concepts (Scenes)/Line Segment 2D.tscn")

static func New (start : Vector2, end : Vector2):
	#var output = SCENE.instantiate()
	var output = LineSegment2D.new()
	output.start = start
	output.end = end
	#GameManager.instance.get_tree().current_scene.add_child(output)
	return output
	#pass

#func _process (deltaTime : float):
	#if drawer != null:
		#drawer.queue_redraw()
#
#func _enter_tree ():
	#if drawer != null:
		#drawer.draw.connect(Draw)
#
#func _exit_tree ():
	#if drawer != null:
		#drawer.draw.disconnect(Draw)
#
#func Draw ():
	#if drawer != null:
		#drawer.draw_line(start, end, drawColor, drawWidth)

func _to_string ():
	return "[" + str(start) + "], [" + str(end) + "]"

func GetSlope ():
	return (end.y - start.y) / (end.x - start.x)

func GetFacingAngle ():
	return VectorExtensions.GetFacingAngle(end - start)

func DoIIntersectWith (other : LineSegment2D, shouldIncludeEndPoints : bool):
	var output = false
	var denominator = (other.end.y - other.start.y) * (end.x - start.x) - (other.end.x - other.start.x) * (end.y - start.y)
	if denominator != 0:
		var u_a = ((other.end.x - other.start.x) * (start.y - other.start.y) - (other.end.y - other.start.y) * (start.x - other.start.x)) / denominator
		var u_b = ((end.x - start.x) * (start.y - other.start.y) - (end.y - start.y) * (start.x - other.start.x)) / denominator
		if shouldIncludeEndPoints:
			if u_a >= 0 && u_a <= 1 && u_b >= 0 && u_b <= 1:
				output = true
		else:
			if u_a > 0 && u_a < 1 && u_b > 0 && u_b < 1:
				output = true
	return output

#public bool DoIIntersectWithCircle (Vector2 center, float radius)
#{
#	return Vector2.Distance(ClosestPoint(center), center) <= radius
#}

#public bool DoIIntersectWithCircle (Vector2 center, float radius)
#{
#	return Vector2.Distance(GetPointWithDirectedDistance(GetDirectedDistanceAlongParallel(center)), center) <= radius
#}

func DoIIntersectWithCircle (center : Vector2, radius : float):
	var lineDirection = GetDirection()
	var centerToLineStart = start - center
	var a = lineDirection.dot(lineDirection)
	var b = 2 * centerToLineStart.dot(lineDirection)
	var c = centerToLineStart.dot(centerToLineStart) - radius * radius
	var discriminant = b * b - 4 * a * c
	if discriminant >= 0:
		discriminant = sqrt(discriminant)
		var t1 = (-b - discriminant) / (2 * a)
		var t2 = (-b + discriminant) / (2 * a)
		if t1 >= 0 && t1 <= 1 || t2 >= 0 && t2 <= 1:
			return true
	return false

func Contains (point : Vector2):
	return (point - start).length() + (point - end).length() == (start - end).length()

func Encapsulates (lineSegment : LineSegment2D):
	return Contains(lineSegment.start) && Contains(lineSegment.end)

func Moved (movement : Vector2):
	return New(start + movement, end + movement)

func RotatedAroundPivot (pivot : Vector2, degrees : float):
	var outputStart = VectorExtensions.RotatedAroundPivot2D(start, pivot, degrees)
	var outputEnd = VectorExtensions.RotatedAroundPivot2D(end, pivot, degrees)
	return New(outputStart, outputEnd)

func GetClosestPoint (point : Vector2):
	var directedDistanceAlongParallel = GetDirectedDistanceAlongParallel(point)
	if directedDistanceAlongParallel > 0 && directedDistanceAlongParallel < GetLength():
		return GetPointWithDirectedDistance(directedDistanceAlongParallel)
	elif directedDistanceAlongParallel >= GetLength():
		return end
	else:
		return start

func GetDistanceSqrTo (point : Vector2):
	return (point - GetClosestPoint(point)).length_squared()

func GetDistanceTo (point : Vector2):
	return (point - GetClosestPoint(point)).length_squared()

func GetPerpendicular (rotateClockwise = false):
	if rotateClockwise:
		return RotatedAroundPivot(GetMidpoint(), -90)
	else:
		return RotatedAroundPivot(GetMidpoint(), 90)

func GetMidpoint ():
	return (start + end) / 2

func GetDirectedDistanceAlongParallel (point : Vector2):
	var rotate = -GetFacingAngle()
	var rotatedLine = RotatedAroundPivot(Vector2.ZERO, rotate)
	point = point.rotated(rotate)
	return point.x - rotatedLine.start.x

func GetPointWithDirectedDistance (directedDistance : float):
	return start + (GetDirection() * directedDistance)

func GetLength ():
	return (start - end).length()

func GetDirection ():
	return (end - start).normalized()

func Flip ():
	return New(end, start)

func GetIntersectionWith (lineSegment : LineSegment2D, collinearOverlapsIntersect = true):
	var intersection = null
	var r = end - start
	var s = lineSegment.end - lineSegment.start
	var rxs = r.cross(s)
	var qpxr = (lineSegment.start - start).cross(r)
	if is_equal_approx(rxs, 0):
		if is_equal_approx(qpxr, 0):
			if collinearOverlapsIntersect && ((0 <= VectorExtensions.Multiply_float(lineSegment.start - start, r) && VectorExtensions.Multiply_float(lineSegment.start - start, r) <= VectorExtensions.Multiply_float(r, r)) || (0 <= VectorExtensions.Multiply_float(lineSegment.start - start, s) && VectorExtensions.Multiply_float(start - lineSegment.start, s) <= VectorExtensions.Multiply_float(start - lineSegment.start, s))):
				return intersection
			else:
				return null
		else:
			return null
	var t = (lineSegment.start - start).cross(s) / rxs
	var u = (lineSegment.start - start).cross(r) / rxs
	if !is_equal_approx(rxs, 0) && (0 <= t && t <= 1) && (0 <= u && u <= 1):
		return start + (t * r)
	return null

func GetYOfPoint (x : float):
	if start.x == end.x:
		push_error("The x-component of the start and end points of the line segment are both " + str(end.x))
	else:
		return GetSlope() * x + GetYIntercept()

func GetXOfPoint (y : float):
	if start.y == end.y:
		push_error("The y-component of the start and end points of the line segment are both " + str(end.y))
	else:
		return (-GetYIntercept() + y) / GetSlope()

func GetXIntercept ():
	var a = start.y - end.y
	var b = start.x - end.x
	if b == 0:
		return start.x
	elif a == 0:
		return INF
	else:
		var slope = GetSlope()
		var c = start.y - slope * start.x
		return -c / slope

func GetYIntercept ():
	var a = start.y - end.y
	var b = start.x - end.x
	if b == 0:
		return INF
	elif a == 0:
		return start.y
	else:
		var slope = GetSlope()
		var c = start.y - slope * start.x
		return c

func GetDistanceSqr (lineSegment : LineSegment2D, checkDistanceInterval = 0.1):
	var output = INF
	var distance = 0.0
	while distance <= GetLength():
		var point = GetPointWithDirectedDistance(distance)
		var distance2 = 0.0
		while distance2 <= GetLength():
			var point2 = lineSegment.GetPointWithDirectedDistance(distance2)
			output = minf((point - point2).length_squared(), output)
			distance2 += checkDistanceInterval
		distance += checkDistanceInterval
	return output

func GetDistance (lineSegment : LineSegment2D, checkDistanceInterval = 0.1):
	return sqrt(GetDistanceSqr(lineSegment, checkDistanceInterval))

func GetClosestPointsTo (lineSegment : LineSegment2D):
	push_error("LineSegment2D.GetClosestPointsTo() not made")
	#Vector2 P1 = start
	#Vector2 P2 = lineSegment.start
	#Vector2 V1 = end - start
	#Vector2 V2 = lineSegment.end - lineSegment.start
	#Vector2 V21 = P2 - P1
	#float v22 = Vector2.Dot(V2, V2)
	#float v11 = Vector2.Dot(V1, V1)
	#float v21 = Vector2.Dot(V2, V1)
	#float v21_1 = Vector2.Dot(V21, V1)
	#float v21_2 = Vector2.Dot(V21, V2)
	#float denom = v21 * v21 - v22 * v11
	#float s
	#float t
	#if (Mathf.Approximately(denom, 0))
	#{
	#	s = 0
	#	t = (v11 * s - v21_1) / v21
	#}
	#else
	#{
	#	s = (v21_2 * v21 - v22 * v21_1) / denom
	#	t = (-v21_1 * v21 + v11 * v21_2) / denom
	#}
	#s = Mathf.Max(Mathf.Min(s, 1), 0)
	#t = Mathf.Max(Mathf.Min(t, 1), 0)
	#Vector2 p_a = P1 + (V1 * s)
	#Vector2 p_b = P2 + (V2 * t)
	#return (p_a, p_b)

#public LineSegment2D[] GetErasedLineSegment (LineSegment2D eraser, Vector2 movement)
#{
#	LineSegment2D[] output = new LineSegment2D[1] { this }
#	LineSegment2D eraserStartMovementLine = new LineSegment2D(eraser.start, eraser.start + movement)
#	LineSegment2D eraserEndMovementLine = new LineSegment2D(eraser.end, eraser.end + movement)
#	Vector2 eraserStartIntersection
#	Vector2 eraserEndIntersection
#	Vector2 eraserStartMovementIntersection
#	Vector2 eraserEndMovementIntersection
#	bool eraserStartIntersects = GetIntersectionWithLineSegment(eraser.start, out eraserStartIntersection)
#	bool eraserEndIntersects = GetIntersectionWithLineSegment(eraser.end, out eraserEndIntersection)
#	bool eraserStartMovementIntersects = GetIntersectionWithLineSegment(eraserStartMovementLine, out eraserStartMovementIntersection)
#	bool eraserEndMovementIntersects = GetIntersectionWithLineSegment(eraserEndMovementLine, out eraserEndMovementIntersection)
#	int intersectionCount = 0
#	if (eraserStartIntersects)
#		intersectionCount ++
#	if (eraserEndIntersects)
#		intersectionCount ++
#	if (eraserStartMovementIntersects)
#		intersectionCount ++
#	if (eraserEndMovementIntersects)
#		intersectionCount ++
#	if (intersectionCount == 2)
#	{
		
#	}
#	else if (intersectionCount == 1)
#	{

#	}
#	else
#	{
		
#	}
#	return output
#}
