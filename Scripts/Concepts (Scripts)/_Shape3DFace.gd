extends Node
class_name _Shape3DFace

@export var normal : Vector3
@export var shapeOnPlane : _Shape2D
@export var corners : PackedVector3Array
@export var edges : Array[LineSegment3D]

static func FromCorners (corners : PackedVector3Array) -> _Shape3DFace:
	var output = new()
	output.corners = corners
	output.SetEdgesFromCorners ()
	output.SetShapeOnPlaneFromCorners ()
	return output

static func FromEdges (edges : Array[LineSegment3D]) -> _Shape3DFace:
	var output = new()
	output.edges = edges
	output.SetCornersFromEdges ()
	output.SetShapeOnPlaneFromCorners ()
	return output

#func DrawGizmos (Color color)
	#for (int i = 0 i < edges.size() i += 1)
		#LineSegment3D edge = edges[i]
		#edge.DrawGizmos (color)

func SetCornersFromShapeOnPlane ():
	corners.clear()
	for i in range(corners.size()):
		var corner = shapeOnPlane.corners[i]
		corners.append(TransformPoint(corner))

func SetEdgesFromShapeOnPlane ():
	edges.clear()
	for i in range(edges.size()):
		var _edge = shapeOnPlane.edges[i]
		edges.append(LineSegment3D.New(TransformPoint(_edge.start), TransformPoint(_edge.end)))

func SetShapeOnPlaneFromCorners ():
	var plane = Plane(corners[0], corners[1], corners[2])
	normal = plane.normal
	var corners : PackedVector2Array
	for corner in self.corners:
		corner = InverseTransformPoint(corner)
		corners.append(Vector2(corner.x, corner.y))
	shapeOnPlane = _Shape2D.PolygonFromCorners(corners)

func SetShapeOnPlaneFromEdges ():
	pass

func SetCornersFromEdges ():
	corners.clear()
	for edge in edges:
		corners.append(edge.end)

func SetEdgesFromCorners ():
	edges.clear()
	var previousCorner = corners[corners.size() - 1]
	for corner in corners:
		edges.append(LineSegment3D.New(previousCorner, corner))
		previousCorner = corner

func Move (movement : Vector3) -> _Shape3DFace:
	var corners : PackedVector3Array
	for i in range(self.corners.size()):
		corners.append(self.corners[i] + movement)
	return FromCorners(corners)

func Rotate (pivotPoint : Vector3, rotation : Quaternion) -> _Shape3DFace:
	var corners : PackedVector3Array
	for corner in self.corners:
		corners.append(VectorExtensions.RotatedAroundPivot3D(corner, pivotPoint, rotation))
	return FromCorners(corners)

func AddSize (add : float) -> _Shape3DFace:
	var averagePoint = GetAveragePoint()
	var corners : PackedVector3Array
	for corner in corners:
		corners.append(corner + ((corner - averagePoint).normalized * add))
	return FromCorners(corners)

func MultiplySize (multiply : float):
	var averagePoint = GetAveragePoint()
	var corners : PackedVector3Array
	for corner in corners:
		corners.append(corner + ((corner - averagePoint) * (multiply - 1)))
	return FromCorners(corners)

func Contains (point : Vector3, equalPointsIntersect : bool = true, checkDistance : float = 99999):
	point = InverseTransformPoint(point)
	return shapeOnPlane.ContainsForPolygon(Vector2(point.x, point.y), equalPointsIntersect, checkDistance)

func DoIIntersectWithLineSegment (lineSegment : LineSegment3D, equalPointsIntersect : bool = true, checkDistance : float = 99999):
	var hitDistance
	var plane = Plane(normal, corners[0])
	var intersection = plane.intersects_ray(lineSegment.start, lineSegment.GetDirection())
	if intersection == null:
		return false
	else:
		hitDistance = (intersection - lineSegment.start).length_squared()
		if hitDistance > lineSegment.GetLength():
			return false
	intersection = InverseTransformPoint(intersection)
	return shapeOnPlane.ContainsForPolygon(intersection, equalPointsIntersect, checkDistance)

func GetIntersectionPointWithLineSegment (lineSegment : LineSegment3D, equalPointsIntersect : bool = true, checkDistance : float = 99999):
	var hitDistance
	var plane = Plane(corners[0], corners[1], corners[2])
	print(str(corners[0]) + ' ' + str(corners[1]) + ' ' + str(corners[2]) + ' ' + str(lineSegment.GetDirection()))
	var intersection = plane.intersects_ray(lineSegment.start, lineSegment.GetDirection())
	if intersection == null:
		return null
	else:
		hitDistance = (intersection - lineSegment.start).length_squared()
		if hitDistance > lineSegment.GetLength() * lineSegment.GetLength():
			return null
	var localHitPoint = InverseTransformPoint(intersection)
	if shapeOnPlane.ContainsForPolygon(Vector2(localHitPoint.x, localHitPoint.y), equalPointsIntersect, checkDistance):
		return intersection
	else:
		return null

func IntersectsFace (face : _Shape3DFace, checkIfContained : bool = true, equalPointsIntersect : bool = true, checkDistance : float = 99999) -> bool:
	return Contains(face.GetRandomPoint(checkIfContained, equalPointsIntersect, checkDistance), equalPointsIntersect, checkDistance) || face.Contains(GetRandomPoint(checkIfContained, equalPointsIntersect, checkDistance), equalPointsIntersect, checkDistance)

func GetRandomPoint (checkIfContained : bool = true, containsEdges : bool = true, checkDistance : float = 99999) -> Vector3:
	return TransformPoint(shapeOnPlane.GetRandomPoint(checkIfContained, containsEdges, checkDistance))

func GetAveragePoint () -> Vector3:
	return VectorExtensions.GetAverage(corners)

func Transform (node : Node3D) -> _Shape3DFace:
	var corners : PackedVector3Array
	for corner in self.corners:
		corners.append(node.to_global(corner))
	return FromCorners(corners)

func InverseTransform (node : Node3D) -> _Shape3DFace:
	var corners : PackedVector3Array
	for corner in self.corners:
		corners.append(node.to_local(corner))
	return FromCorners(corners)

func TransformPoint (localPoint : Vector3) -> Vector3:
	var averagePoint = GetAveragePoint()
	localPoint = VectorExtensions.RotatedAroundPivot3D(localPoint, Vector3.ZERO, Quaternion(Vector3.FORWARD, normal))
	localPoint += averagePoint
	return localPoint

func InverseTransformPoint (worldPoint : Vector3) -> Vector3:
	var averagePoint = GetAveragePoint()
	worldPoint -= averagePoint
	worldPoint = VectorExtensions.RotatedAroundPivot3D(worldPoint, Vector3.ZERO, Quaternion(Vector3.FORWARD, -normal))
	return worldPoint

func GetPerimeter () -> float:
	var output = 0.0
	for edge in edges:
		output += edge.GetLength()
	return output

func GetPointOnPerimeter (distance : float) -> Vector3:
	while true:
		for edge in edges:
			var edgeLength = edge.GetLength()
			distance -= edgeLength
			if distance <= 0:
				return edge.GetPointWithDirectedDistance(edgeLength + distance)
	return Vector3()

func GetClosestPoint (point : Vector3, checkDistance : float = 99999) -> Vector3:
	var closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance)
	return closestPointAndDistanceSqr.point

func GetDistanceSqr (point : Vector3, checkDistance : float = 99999) -> Vector3:
	var closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance)
	return closestPointAndDistanceSqr.distanceSqr

func GetClosestPointAndDistanceSqr (point : Vector3, checkDistance : float = 99999):
	var localPoint = InverseTransformPoint(point)
	var closestPointAndDistanceSqr = shapeOnPlane.GetClosestPointAndDistanceSqr(localPoint, checkDistance)
	return [ TransformPoint(closestPointAndDistanceSqr[0]), closestPointAndDistanceSqr[1] ]

func HasSamePoints (face : _Shape3DFace):
	if corners.size() != face.corners.size():
		return false
	for corner in corners:
		if not corner in corners:
			return false
	return true
