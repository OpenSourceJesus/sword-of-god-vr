extends Node
class_name ColorPalette

@export var redCurve : Curve
@export var greenCurve : Curve
@export var blueCurve : Curve

func Get (value : float) -> Color:
	return Color(redCurve.sample(value), greenCurve.sample(value), blueCurve.sample(value))
