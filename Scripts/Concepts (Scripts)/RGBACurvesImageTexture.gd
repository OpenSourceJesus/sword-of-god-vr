extends ImageTexture
class_name RGBACurvesImageTexture

@export var size : Vector2i
@export var rXCurve = Curve.new()
@export var gXCurve = Curve.new()
@export var bXCurve = Curve.new()
@export var aXCurve = Curve.new()
@export var rYCurve = Curve.new()
@export var gYCurve = Curve.new()
@export var bYCurve = Curve.new()
@export var aYCurve = Curve.new()
@export var image : Image

func Update ():
	image = Image.create(size.x, size.y, false, Image.Format.FORMAT_RGBAF)
	for x in range(size.x):
		for y in range(size.y):
			var xSamplePoint = float(x) / size.x
			var xColor = Color(rXCurve.sample(xSamplePoint), gXCurve.sample(xSamplePoint), bXCurve.sample(xSamplePoint), aXCurve.sample(xSamplePoint))
			var ySamplePoint = float(y) / size.y
			var yColor = Color(rYCurve.sample(ySamplePoint), gYCurve.sample(ySamplePoint), bYCurve.sample(ySamplePoint), aYCurve.sample(ySamplePoint))
			image.set_pixel(x, y, xColor + yColor / 2)
	update(image)
	ResourceSaver.save(self)
