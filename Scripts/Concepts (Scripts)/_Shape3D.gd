extends Resource
class_name _Shape3D

@export var corners : PackedVector3Array
@export var edges : Array
@export var faces : Array

static func FromFaces (faces : Array[_Shape3DFace]) -> _Shape3D:
	var output = new()
	output.faces = faces
	output.SetEdgesFromFaces ()
	output.SetCornersFromEdges ()
	return output

static func Copy (shape : _Shape3D, deepCopy : bool = false) -> _Shape3D:
	var output = new()
	output.corners = shape.corners.duplicate()
	output.edges = shape.edges.duplicate(deepCopy)
	output.faces = shape.faces.duplicate(deepCopy)
	return output

#func DrawGizmos (Color color)
	#for (int i = 0 i < edges.Length i += 1)
		#LineSegment3D edge = edges[i]
		#edge.DrawGizmos (color)

func SetCornersFromEdges ():
	var corners : PackedVector3Array
	for edge in edges:
		if not edge.end in corners:
			corners.append(edge.end)
	self.corners = corners

func SetCornersFromFaces ():
	pass

func SetEdgesFromFaces ():
	var edges : Array[LineSegment3D]
	for face in faces:
		for edge in face.edges:
			if not edge in edges && not edge.Flip() in edges:
				edges.append(edge)
	self.edges = edges

#func Transform (node : Node3D) -> _Shape3D:
	#_Shape3DFace[] faces = _Shape3DFace[self.faces.Length]
	#for (int i = 0 i < faces.Length i += 1)
		#faces[i] = self.faces[i].Transform(node)
	#return _Shape3D(faces)
#
#_Shape3D InverseTransform (Transform node)
	#_Shape3DFace[] faces = _Shape3DFace[self.faces.Length]
	#for (int i = 0 i < faces.Length i += 1)
		#faces[i] = self.faces[i].InverseTransform(node)
	#return _Shape3D(faces)
#
#_Shape3D Move (Vector3 movement)
	#_Shape3DFace[] faces = _Shape3DFace[self.faces.Length]
	#for (int i = 0 i < faces.Length i += 1)
		#faces[i] = self.faces[i].Move(movement)
	#return _Shape3D(faces)
#
#_Shape3D Rotate (Vector3 pivotPoint, Quaternion rotation)
	#_Shape3DFace[] faces = _Shape3DFace[self.faces.Length]
	#for (int i = 0 i < faces.Length i += 1)
		#faces[i] = self.faces[i].Rotate(pivotPoint, rotation)
	#return _Shape3D(faces)
#
#_Shape3D AddSize (float add)
	#_Shape3DFace[] faces = _Shape3DFace[self.faces.Length]
	#for (int i = 0 i < faces.Length i += 1)
		#faces[i] = self.faces[i].AddSize(add)
	#return _Shape3D(faces)
#
#_Shape3D MultiplySize (float multiply)
	#_Shape3DFace[] faces = _Shape3DFace[self.faces.Length]
	#for (int i = 0 i < faces.Length i += 1)
		#faces[i] = self.faces[i].MultiplySize(multiply)
	#return _Shape3D(faces)
#
#float GetPerimeter ()
	#float output = 0
	#for (int i = 0 i < edges.Length i += 1)
		#LineSegment3D edge = edges[i]
		#output += edge.GetLength()
	#return output
#
#Vector3 GetPointOnPerimeter (float distance)
	#while (true)
		#for (int i = 0 i < edges.Length i += 1)
			#LineSegment3D edge = edges[i]
			#float edgeLength = edge.GetLength()
			#distance -= edgeLength
			#if (distance <= 0)
				#return edge.GetPointWithDirectedDistance(edgeLength + distance)

func Contains (point : Vector3, equalPointsIntersect : bool = true, checkDistance : float = 99999) -> bool:
	var checkLineSegment = LineSegment3D.New(point, point + (VectorExtensions.Random3D(FloatRange.New(1, 1)) * checkDistance))
	var collisionCount = 0
	for face in faces:
		if face.DoIIntersectWithLineSegment(checkLineSegment, equalPointsIntersect, checkDistance):
			collisionCount += 1
	return collisionCount % 2 == 1

func DoIIntersectWithLineSegment (lineSegment : LineSegment3D, equalPointsIntersect : bool = true, checkDistance : float = 99999):
	if Contains(lineSegment.start, equalPointsIntersect, checkDistance) || Contains(lineSegment.end, equalPointsIntersect, checkDistance):
		return true
	for face in faces:
		if face.DoIIntersectWithLineSegment(lineSegment, equalPointsIntersect, checkDistance):
			return true
	return false

func GetIntersectionPointWithLineSegment (lineSegment : LineSegment3D, equalPointsIntersect : bool = true, checkDistance : float = 99999) -> Array:
	for face in faces:
		var output = face.GetIntersectionPointWithLineSegment(lineSegment, equalPointsIntersect, checkDistance)
		if output.size() > 0:
			return output
	return []

func GetFaceAndIntersectionPointWithLineSegment (lineSegment : LineSegment3D, equalPointsIntersect : bool = true, checkDistance : float = 99999) -> Array:
	for face in faces:
		var output = face.GetIntersectionPointWithLineSegment(lineSegment, equalPointsIntersect, checkDistance)
		if output.size() > 0:
			return [ face, output[0] ]
	return []

#func GetRandomPointOnEdges () -> Vector3:
	#return GetPointOnPerimeter(randf() * GetPerimeter())

func GetRandomPoint (checkIfContained : bool = true, containsFaces : bool = true, checkDistance : float = 99999) -> Vector3:
	while true:
		var face1 = faces[randi_range(0, faces.size())]
		var face2 = faces[randi_range(0, faces.size())]
		var point1 = face1.GetRandomPoint(checkIfContained, containsFaces, checkDistance)
		var point2 = face2.GetRandomPoint(checkIfContained, containsFaces, checkDistance)
		var output = (point1 + point2) / 2
		if !checkIfContained || Contains(output, containsFaces, checkDistance):
			return output
	return Vector3()

func GetClosestPoint (point : Vector3, checkDistance : float = 99999) -> Vector3:
	var closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance)
	return closestPointAndDistanceSqr[0]

func GetDistanceSqr (point : Vector3, checkDistance : float = 99999) -> float:
	var closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance)
	return closestPointAndDistanceSqr[1]

func GetClosestPointAndDistanceSqr (point : Vector3, checkDistance : float = 99999) -> Array:
	var closestPoint = Vector3()
	var closestDistanceSqr = INF
	for face in faces:
		var closestPointAndDistanceSqr = face.GetClosestPointAndDistanceSqr(point, checkDistance)
		if closestPointAndDistanceSqr.distanceSqr < closestDistanceSqr:
			closestDistanceSqr = closestPointAndDistanceSqr.distanceSqr
			closestPoint = closestPointAndDistanceSqr.point
	return []

func MergeForPorEnlcosed (shape : _Shape3D, assumeSharedFaces : int = 99999):
	var outputFaces = faces.duplicate(false)
	outputFaces.append_array(shape.faces)
	if assumeSharedFaces > 0:
		for face in faces:
			for face2 in faces:
				if face.HasSamePoints(face2):
					outputFaces.erase(face)
					assumeSharedFaces -= 1
					if assumeSharedFaces <= 0:
						return FromFaces(outputFaces)
	return FromFaces(outputFaces)

func BooleanOnto (shapeToBoolean : _Shape3D):
	pass

func SetCorner (cornerIndex : int, newPosition : Vector3):
	var faces = self.faces.duplicate(false)
	var corner = corners[cornerIndex]
	for i in range(faces.size()):
		var face = faces[i]
		var cornerIndexInFace = face.corners.find(corner)
		if cornerIndexInFace != -1:
			face.corners[cornerIndexInFace] = newPosition
			face = _Shape3DFace.FromCorners(face.corners)
		faces[i] = face
	return FromFaces(faces)

func ToMesh () -> Mesh:
	var output = Mesh.new()
	output.vertices = corners.duplicate()
	var oldFaces = self.faces.duplicate(false)
	var faces = self.faces.duplicate(false)
	var triangles : PackedInt32Array
	for i in range(faces.size() * 3):
		triangles.append(0)
	var facesCornerCounts : Array[int]
	for face in facesCornerCounts:
		facesCornerCounts.append(0)
	for i in range(corners.size()):
		var corner = corners[i]
		for i2 in range(faces.size()):
			var face = faces[i2]
			var cornerIndex = face.corners.find(corner)
			if cornerIndex > -1:
				if facesCornerCounts[i2] < 3:
					facesCornerCounts[i2] += 1
				else:
					faces.remove_at(i2)
					facesCornerCounts.remove_at(i2)
					i2 -= 1
				triangles[oldFaces.find(face) * 3 + cornerIndex] = i
	output.SetTriangles(triangles, 0)
	return output

static func FromMesh (mesh : Mesh) -> _Shape3D:
	var meshData = mesh.surface_get_arrays(0)
	var vertices = meshData[0]
	var indices = meshData[12]
	var faces : Array[_Shape3DFace]
	for i in range(indices.size() / 3):
		faces.append(_Shape3DFace.FromCorners([]))
	for i in range(0, indices.size(), 3):
		var vertexIndex1 = indices[i]
		var vertexIndex2 = indices[i + 1]
		var vertexIndex3 = indices[i + 2]
		var vertex1 = vertices[vertexIndex1]
		var vertex2 = vertices[vertexIndex2]
		var vertex3 = vertices[vertexIndex3]
		faces[i / 3] = _Shape3DFace.FromCorners([vertex1, vertex2, vertex3])
	return FromFaces(faces)

static func FromAABB (bounds : AABB) -> _Shape3D:
	var output = FromFaces([])
	var corners : PackedVector3Array
	corners.append(bounds.position)
	corners.append(Vector3(bounds.end.x, bounds.position.y, bounds.position.z))
	corners.append(Vector3(bounds.end.x, bounds.position.y, bounds.end.z))
	corners.append(Vector3(bounds.position.x, bounds.position.y, bounds.end.z))
	output.faces.append(_Shape3DFace.FromCorners(corners)) # bottom
	corners[0] = Vector3(bounds.position.x, bounds.end.y, bounds.position.z)
	corners[1] = Vector3(bounds.end.x, bounds.end.y, bounds.position.z)
	corners[3] = Vector3(bounds.position.x, bounds.end.y, bounds.end.z)
	corners[2] = Vector3(bounds.end.x, bounds.end.y, bounds.end.z)
	output.faces.append(_Shape3DFace.FromCorners(corners)) # top
	corners[0] = Vector3(bounds.position.x, bounds.position.y, bounds.position.z)
	corners[1] = Vector3(bounds.position.x, bounds.end.y, bounds.position.z)
	corners[3] = Vector3(bounds.position.x, bounds.end.y, bounds.end.z)
	corners[2] = Vector3(bounds.position.x, bounds.position.y, bounds.end.z)
	output.faces.append(_Shape3DFace.FromCorners(corners)) # left
	corners[0] = Vector3(bounds.end.x, bounds.position.y, bounds.position.z)
	corners[1] = Vector3(bounds.end.x, bounds.end.y, bounds.position.z)
	corners[3] = Vector3(bounds.end.x, bounds.end.y, bounds.end.z)
	corners[2] = Vector3(bounds.end.x, bounds.position.y, bounds.end.z)
	output.faces.append(_Shape3DFace.FromCorners(corners)) # right
	corners[0] = Vector3(bounds.position.x, bounds.position.y, bounds.position.z)
	corners[1] = Vector3(bounds.end.x, bounds.position.y, bounds.position.z)
	corners[3] = Vector3(bounds.end.x, bounds.end.y, bounds.position.z)
	corners[2] = Vector3(bounds.position.x, bounds.end.y, bounds.position.z)
	output.faces[1] = _Shape3DFace.FromCorners(corners) # back
	corners[0] = Vector3(bounds.position.x, bounds.position.y, bounds.end.z)
	corners[1] = Vector3(bounds.end.x, bounds.position.y, bounds.end.z)
	corners[3] = Vector3(bounds.end.x, bounds.end.y, bounds.end.z)
	corners[2] = Vector3(bounds.position.x, bounds.end.y, bounds.end.z)
	output.faces[1] = _Shape3DFace.FromCorners(corners) # front
	return output
