extends Node
class_name FloatRange

@export var min : float
@export var max : float

static func New (min : float, max : float) -> FloatRange:
	var output = new()
	output.min = min
	output.max = max
	return output

func Contains (value : float, containsMin = true, containsMax = true):
	var greaterThanMin = min < value
	if containsMin && min == value:
		greaterThanMin = true
	var lessThanMax = value < max
	if containsMax && value == max:
		lessThanMax = true
	return greaterThanMin && lessThanMax

func Get (normalizedValue : float) -> float:
	return lerp(min, max, normalizedValue)
