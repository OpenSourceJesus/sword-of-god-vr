extends GodEffect
class_name TemporarilyMultiplyDamageGodEffect

@export var amount : float
@export var interval : float
@export var duration : float
var timeUntilMultiplyDamage : float
var durationRemaining : float

func _ready ():
	set_process(false)

func Apply (points : int):
	super.Apply (points)
	set_process(true)

func Unapply (points : int):
	super.Unapply (points)
	set_process(false)

func _process (deltaTime : float):
	timeUntilMultiplyDamage -= deltaTime
	if timeUntilMultiplyDamage <= 0:
		%Player.multiplyDamage *= amount
		%Player.extraDamageIndicator.visible = true
		durationRemaining = duration
		timeUntilMultiplyDamage = interval
	elif %Player.invulnerable:
		durationRemaining -= deltaTime
		if durationRemaining <= 0:
			durationRemaining = INF
			%Player.multiplyDamage /= amount
			%Player.extraDamageIndicator.visible = false

func GetDescription ():
	var output = "Multiplies your regular attack damage and radiation damage by " + str(amount) + " every interval of " + str(interval) + " seconds for a duration of " + str(duration) + " seconds"
	for godEffects in Player.instance.godEffectsDict.values():
		for godEffect in godEffects:
			var turnInvulernableEachTimeIntervalForDurationGodEffect = godEffect as TurnInvulernableEachTimeIntervalForDurationGodEffect
			if turnInvulernableEachTimeIntervalForDurationGodEffect != null:
				output += "\nThe timing of this effect is aligned with the timing of your temporary invulnerability effect"
	return output
