extends GodEffect
class_name ReviveWithLessCashGodEffect

@export var loseCashAmountOnRevive : int
@export var capDifferenceOnRevive : int

func GetDescription ():
	return "Each time you die you revive, but you revive with " + str(loseCashAmountOnRevive) +  " less materials, and your materials are capped at " + str(capDifferenceOnRevive) + " under the amount you revived with. You lose after getting to 0 cash."
