extends GodEffect
class_name AddToEnemyCashMultiplierGodEffect

@export var amount : float
var addToEnemyCashMultiplier : float

func Apply (points : int):
	super.Apply (points)
	if scalesWithPoints:
		Player.instance.SetEnemyCashMultiplier (Player.instance.enemyCashMultiplier - addToEnemyCashMultiplier)
		if subtractPointsRequired:
			addToEnemyCashMultiplier = amount * (points - pointsRequired)
		else:
			addToEnemyCashMultiplier = amount * points
		Player.instance.SetEnemyCashMultiplier (Player.instance.enemyCashMultiplier + addToEnemyCashMultiplier)
	else:
		Player.instance.SetEnemyCashMultiplier (Player.instance.enemyCashMultiplier + amount)

func Unapply (points : int):
	super.Unapply (points)
	if scalesWithPoints:
		Player.instance.SetEnemyCashMultiplier (Player.instance.enemyCashMultiplier + addToEnemyCashMultiplier)
		if subtractPointsRequired:
			addToEnemyCashMultiplier = amount * (points - pointsRequired)
		else:
			addToEnemyCashMultiplier = amount * points
		Player.instance.SetEnemyCashMultiplier (Player.instance.enemyCashMultiplier - addToEnemyCashMultiplier)
	else:
		Player.instance.SetEnemyCashMultiplier (Player.instance.enemyCashMultiplier - amount)

func GetDescription ():
	if scalesWithPoints:
		var output = ""
		if !subtractPointsRequired:
			output = "Enemies now drop " + str(amount * pointsRequired / Player.instance.previousEnemyCashMultiplier) + "x more materials\n"
		output += "Each time you get a favor point from this god enemies will drop " + str(amount) + "x more materials"
	else:
		return "Enemies now drop " + str(amount / Player.instance.previousEnemyCashMultiplier) + "x more materials"
