extends GodEffect
class_name AddToDamageGodEffect

@export var amount : float
var addToDamage : float

func Apply (points : int):
	super.Apply (points)
	if scalesWithPoints:
		Player.instance.damage -= addToDamage
		if subtractPointsRequired:
			addToDamage = amount * (points - pointsRequired)
		else:
			addToDamage = amount * points
		Player.instance.damage += addToDamage
	else:
		Player.instance.damage += amount

func Unapply (points : int):
	super.Unapply (points)
	if scalesWithPoints:
		Player.instance.damage += addToDamage
		if subtractPointsRequired:
			addToDamage = amount * (points - pointsRequired)
		else:
			addToDamage = amount * points
		Player.instance.damage -= addToDamage
	else:
		Player.instance.damage -= amount

func GetDescription ():
	if scalesWithPoints:
		var output = ""
		if !subtractPointsRequired:
			output += "Gain " + str(amount * pointsRequired) + " regular attack damage\n"
		output += "Each time you get a favor point from this god you will gain " + str(amount) + " regular attack damage"
	else:
		return "Gain " + str(amount) + " regular attack damage"
