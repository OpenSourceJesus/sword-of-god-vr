extends GodEffect
class_name AddToRadiationDamageGodEffect

@export var amount : float
var addToRadiationDamage : float

func Apply (points : int):
	super.Apply (points)
	if scalesWithPoints:
		Player.instance.radiationDamage -= addToRadiationDamage
		if subtractPointsRequired:
			addToRadiationDamage = amount * (points - pointsRequired)
		else:
			addToRadiationDamage = amount * points
		Player.instance.radiationDamage += addToRadiationDamage
	else:
		Player.instance.radiationDamage += amount

func Unapply (points : int):
	super.Unapply (points)
	if scalesWithPoints:
		Player.instance.radiationDamage += addToRadiationDamage
		if subtractPointsRequired:
			addToRadiationDamage = amount * (points - pointsRequired)
		else:
			addToRadiationDamage = amount * points
		Player.instance.radiationDamage -= addToRadiationDamage
	else:
		Player.instance.radiationDamage -= amount

func GetDescription ():
	var output = ""
	if scalesWithPoints:
		if !subtractPointsRequired:
			output = "Gain " + str(amount * pointsRequired) + " radiation damage\n"
		output += "Each time you get a favor point from this god you will gain " + str(amount) + " radiation damage\n"
	else:
		output = "Gain " + str(amount) + " radiation damage\n"
	output += "Radiation does constant damage over time to each enemy in the room based on your distance to them\nAt " + str(Player.instance.radiationDistanceScale) + " or greater distance radiation does 0 damage, and at 0 distance radiation does full damage"
	return output
