extends GodEffect
class_name AddToCashEachRoomGodEffect

@export var amount : int

func GetDescription ():
	return "Gain " + str(amount) + " materials after every room you clear of enemies"
