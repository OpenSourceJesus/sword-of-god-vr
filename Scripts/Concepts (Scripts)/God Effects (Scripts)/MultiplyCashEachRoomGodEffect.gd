extends GodEffect
class_name MultiplyCashEachRoomGodEffect

@export var amount : float

func GetDescription ():
	return "Multiplies your materials by " + str(amount) + " after every room you clear of enemies"
