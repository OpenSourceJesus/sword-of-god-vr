extends GodEffect
class_name ReviveWithLessMaxHpGodEffect

func GetDescription ():
	return "Each time you die you revive, but you revive with one less max health. You lose after getting to 0."
