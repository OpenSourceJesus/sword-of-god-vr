extends GodEffect
class_name AddToAttackSpeedGodEffect

@export var amount : float
var addToAttackSpeed : float

func Apply (points : int):
	super.Apply (points)
	if scalesWithPoints:
		Player.instance.attackSpeed -= addToAttackSpeed
		if subtractPointsRequired:
			addToAttackSpeed = amount * (points - pointsRequired)
		else:
			addToAttackSpeed = amount * points
		Player.instance.attackSpeed += addToAttackSpeed
	else:
		Player.instance.attackSpeed += amount

func Unapply (points : int):
	super.Unapply (points)
	if scalesWithPoints:
		Player.instance.attackSpeed += addToAttackSpeed
		if subtractPointsRequired:
			addToAttackSpeed = amount * (points - pointsRequired)
		else:
			addToAttackSpeed = amount * points
		Player.instance.attackSpeed -= addToAttackSpeed
	else:
		Player.instance.attackSpeed -= amount

func GetDescription ():
	if scalesWithPoints:
		var output = ""
		if !subtractPointsRequired:
			output += "Gain " + str(amount * pointsRequired) + " attack speed\n"
		output += "Each time you get a favor point from this god you will gain " + str(amount) + " attack speed"
	else:
		return "Gain " + str(amount) + " attack speed"
