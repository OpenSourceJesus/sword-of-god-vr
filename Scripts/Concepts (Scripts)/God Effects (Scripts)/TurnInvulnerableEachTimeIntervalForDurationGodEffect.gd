extends GodEffect
class_name TurnInvulernableEachTimeIntervalForDurationGodEffect

@export var interval : float
@export var duration : float
var timeUntilInvulnerable : float
var durationRemaining : float

func _ready ():
	set_process(false)

func Apply (points : int):
	super.Apply (points)
	set_process(true)

func Unapply (points : int):
	super.Unapply (points)
	set_process(false)

func _process (deltaTime : float):
	timeUntilInvulnerable -= deltaTime
	if timeUntilInvulnerable <= 0:
		%Player.invulnerable = true
		%Player.invulnerableIndicator.visible = true
		durationRemaining = duration
		timeUntilInvulnerable = interval
	elif %Player.invulnerable:
		durationRemaining -= deltaTime
		if durationRemaining <= 0:
			durationRemaining = INF
			%Player.invulnerable = false
			%Player.invulnerableIndicator.visible = false

func GetDescription ():
	var output = "You turn invulenerable every interval of " + str(interval) + " seconds for a duration of " + str(duration) + " seconds"
	for godEffects in Player.instance.godEffectsDict.values():
		for godEffect in godEffects:
			var temporarilyMultiplyDamageGodEffect = godEffect as TemporarilyMultiplyDamageGodEffect
			if temporarilyMultiplyDamageGodEffect != null:
				output += "\nThe timing of this effect is aligned with the timing of your temporarily multiplied damage effect"
	return output
