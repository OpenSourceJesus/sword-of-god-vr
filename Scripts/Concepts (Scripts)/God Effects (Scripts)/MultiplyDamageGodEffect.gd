extends GodEffect
class_name MultiplyDamageGodEffect

@export var amount : float
var multiplyDamage : float = 1

func Apply (points : int):
	super.Apply (points)
	Player.instance.damage *= amount

func Unapply (points : int):
	super.Unapply (points)
	Player.instance.damage /= amount

func GetDescription ():
	return "Your regular attack damage is multiplied by " + str(amount)
