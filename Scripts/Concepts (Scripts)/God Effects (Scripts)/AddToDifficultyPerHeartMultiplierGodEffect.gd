extends GodEffect
class_name AddToDifficultyPerHeartMultiplierGodEffect

@export var amount : float

func Apply (points : int):
	super.Apply (points)
	Game.instance.multiplyDifficultyTillHeart += amount

	super.Unapply (points)
	Game.instance.multiplyDifficultyTillHeart -= amount

func GetDescription ():
	return "Enemies drop hearts " + str((Game.instance.multiplyDifficultyTillHeart + amount) / Game.instance.multiplyDifficultyTillHeart) + "x more often"
