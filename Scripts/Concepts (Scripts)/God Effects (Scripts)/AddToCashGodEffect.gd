extends GodEffect
class_name AddToCashGodEffect

@export var amount : float

func Apply (points : int):
	super.Apply (points)
	Player.instance.AddCash (amount)

func Unapply (points : int):
	super.Unapply (points)
	Player.instance.AddCash (-amount)

func GetDescription ():
	return "Gain " + str(amount) + " cash"
