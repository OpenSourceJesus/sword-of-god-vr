extends GodEffect
class_name AddToMaxHpGodEffect

@export var amount : float
var addToMaxHp : float

func Apply (points : int):
	super.Apply (points)
	if scalesWithPoints:
		Player.instance.maxHp -= addToMaxHp
		if subtractPointsRequired:
			addToMaxHp = amount * (points - pointsRequired)
		else:
			addToMaxHp = amount * points
		Player.instance.maxHp += addToMaxHp
	else:
		Player.instance.maxHp += amount
	Player.instance.UpdateMaxHpImages ()

func Unapply (points : int):
	super.Unapply (points)
	if scalesWithPoints:
		Player.instance.maxHp += addToMaxHp
		if subtractPointsRequired:
			addToMaxHp = amount * (points - pointsRequired)
		else:
			addToMaxHp = amount * points
		Player.instance.maxHp -= addToMaxHp
	else:
		Player.instance.maxHp -= amount
	Player.instance.UpdateMaxHpImages ()

func GetDescription ():
	if scalesWithPoints:
		var output = ""
		if !subtractPointsRequired:
			output += "Gain " + str(amount * pointsRequired) + " max health\n"
		output += "Each time you get a favor point from this god you will gain " + str(amount) + " max health"
	else:
		return "Gain " + str(amount) + " max health"
