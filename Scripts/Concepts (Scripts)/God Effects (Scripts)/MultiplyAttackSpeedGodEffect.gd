extends GodEffect
class_name MultiplyAttackSpeedGodEffect

@export var amount : float
var multiplyAttackSpeed : float = 1

func Apply (points : int):
	super.Apply (points)
	Player.instance.attackSpeed *= amount

func Unapply (points : int):
	super.Unapply (points)
	Player.instance.attackSpeed /= amount

func GetDescription ():
	return "Your attack speed is multiplied by " + str(amount)
