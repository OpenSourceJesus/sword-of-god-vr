extends GodEffect
class_name AddToMaxAttackBounces

@export var amount : float
var addToMaxAttackBounces : float

func Apply (points : int):
	super.Apply (points)
	if scalesWithPoints:
		Player.instance.maxAttackBounces -= addToMaxAttackBounces
		if subtractPointsRequired:
			addToMaxAttackBounces = amount * (points - pointsRequired)
		else:
			addToMaxAttackBounces = amount * points
		Player.instance.maxAttackBounces += addToMaxAttackBounces
	else:
		Player.instance.maxAttackBounces += amount

func Unapply (points : int):
	super.Unapply (points)
	if scalesWithPoints:
		Player.instance.maxAttackBounces += addToMaxAttackBounces
		if subtractPointsRequired:
			addToMaxAttackBounces = amount * (points - pointsRequired)
		else:
			addToMaxAttackBounces = amount * points
		Player.instance.maxAttackBounces -= addToMaxAttackBounces
	else:
		Player.instance.maxAttackBounces -= amount

func GetDescription ():
	var output = ""
	if scalesWithPoints:
		if !subtractPointsRequired:
			if amount * pointsRequired == 1:
				output = "Your regular attack can bounce to one more enemy\n"
			else:
				output = "Your regular attack can bounce to " + str(amount * pointsRequired) + " more enemies\n"
		if amount == 1:
			output += "Each time you get a favor point from this god your regular attack can bounce to one more enemy"
		else:
			output += "Each time you get " + str(1 / amount) + " favor points from this god your regular attack can bounce to one more enemy\n"
	else:
		if amount == 1:
			output = "Your regular attack can bounce to one more enemy\n"
		else:
			output = "Your regular attack can bounce to " + str(amount) + " more enemies\n"
	output += "A bounce can't be made if it is over your regular attack's range\nEach bounce reduces the range of future bounces from the same regular attack\nA bounce can't reach an enemy already hit by the same regular attack"
	return output
