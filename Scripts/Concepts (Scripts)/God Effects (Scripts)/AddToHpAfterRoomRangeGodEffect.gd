extends GodEffect
class_name AddToHpAfterRoomRangeGodEffect

@export var amount : int
@export var roomRange : IntRange
var roomsRemaining : int

func Apply (points : int):
	super.Apply (points)
	roomsRemaining = randi_range(roomRange.min, roomRange.max)

func GetDescription ():
	if amount > 0:
		return "Heal " + str(amount) + " health after every " + str(roomRange.min) + " to " + str(roomRange.max) + " rooms you clear of enemies"
	else:
		return "Take " + str(amount) + " damage after every " + str(roomRange.min) + " to " + str(roomRange.max) + " rooms you clear of enemies"
