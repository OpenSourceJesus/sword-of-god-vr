@tool
extends Node
class_name _Curve2D

@export var points : Array[_Curve2DPoint]
@export var fillColor : Color
@export var curve = Curve2D.new()
@export var centers : Array[_Curve2DCenter]
@export var pointsParent : Node
@export var centersParent : Node
@export var extraChecksPerPoint : int

static func New (points : Array[_Curve2DPoint], fillColor : Color):
	var output = _Curve2D.new()
	output.points = points
	output.fillColor = fillColor
	output.Update ()
	return output

func Update ():
	points.clear()
	points.append_array(pointsParent.get_children())
	centers.clear()
	centers.append_array(centersParent.get_children())
	curve = Curve2D.new()
	for point in points:
		curve.add_point(point.global_position, point.inControlPoint.position, point.outControlPoint.position)
