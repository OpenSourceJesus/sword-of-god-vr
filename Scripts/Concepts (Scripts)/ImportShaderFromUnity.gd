extends Node
class_name ImportShaderFromUnity

@export var importPath : String
@export var exportPath : String
@export var assetsPath : String
var importFileContent : String
var properties : Array[Property]
var definesDict : Dictionary
var includes : Array[String]
var packagesCachePath : String
var exportFileContent : String
var inComment : bool
var line : String
var packagesfilePath : String
var commentIndicatorIndex : int
var startMultilineCommentIndicatorIndex : int
var endMultilineCommentIndicatorIndex : int
var inCommentDict : Dictionary
var exportLineCount : int
const PRAGMA_TARGET_INDIDCATOR = "#pragma target "
const MAX_EXPORT_LINES = 5999
const PROPERTIES_START_INDICATOR = "Properties"
const PROPERTIES_END_INDICATOR = "    }"
const PROPERTY_NAME_START_INDICATOR = "_"
const PROPERTY_NAME_END_INDICATOR = "("
const PROPERTY_TYPE_START_INDICATOR = ", "
const PROPERTY_TYPE_END_INDICATOR = ")"
const PROPERTY_VALUE_START_INDICATOR = "= "
const PROPERTY_VALUE_END_INDICATOR = "\n"
const VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR = ", "
const HLSL_START_INDICATOR = "HLSLPROGRAM"
const HLSL_END_INDICATOR = "ENDHLSL"
const MACRO_ARGUMENTS_START_INDICATOR = "("
const DEFINE_INDICATOR = "#define "
const UNDEFINE_UNITY_INDICATOR = "#undefine "
const UNDEFINE_GODOT_INDICATOR = "#undef "
const IF_DIRECTIVE_INDICATOR = "#if "
const IF_DEFINED_INDICATOR = "#ifdef "
const IF_UNDEFINED_INDICATOR = "#ifndef "
const END_IF_INDICATOR = "#endif"
const ELSE_DIRECTIVE_INDICATOR = "#else"
const ELSE_IF_DIRECTIVE_INDICATOR = "#elif "
const IF_INDICATOR = "if ("
const DEFINED_START_INDICATOR = "defined("
const DEFINED_END_INDICATOR = ")"
const INCLUDE_START_INDICATOR = "#include \"Packages/"
const END_IF_DIRECTIVE_INDICATOR = "#endif"
const RETURN_INDICATOR = "return "
const COMMENT_INDICATOR = "// "
const MULTILINE_COMMENT_START_INDICATOR = "/* "
const MULTILINE_COMMENT_END_INDICATOR = " */"
const AND_INDICATOR = "&&"
const OR_INDICATOR = "||"
const TRUE_INDICATOR = "true"
const FALSE_INDICATOR = "false"

func _ready ():
	#Do ()
	pass

func Do ():
	var assetsIndicatorIndex = assetsPath.find("Assets")
	packagesCachePath = assetsPath.substr(0, assetsIndicatorIndex) + "Library/PackageCache"
	packagesfilePath = importPath.replace(packagesCachePath + "/", "")
	var importFile = FileAccess.open(importPath, FileAccess.READ)
	importFileContent = importFile.get_as_text()
	importFileContent = importFileContent.substr(importFileContent.find(PROPERTIES_START_INDICATOR))
	exportFileContent = "shader_type canvas_item;"
	definesDict["SHADER_API_MOBILE"] = 1
	definesDict["SHADER_API_GLES3"] = 0
	definesDict["REAL_IS_HALF"] = 0
	for defineName in definesDict:
		exportFileContent += "\n" + DEFINE_INDICATOR + defineName + " " + str(definesDict[defineName])
	exportLineCount += 1
	while !importFileContent.begins_with(PROPERTIES_END_INDICATOR):
		var propertyName = ExtractPropertyName()
		var propertyType = ExtractPropertyType()
		if propertyType == null:
			break
		var propertyValue = ExtractPropertyValue(propertyType)
		var property = Property.New(propertyName, propertyType, propertyValue)
		properties.append(property)
		importFileContent = importFileContent.substr(PROPERTIES_END_INDICATOR.length())
	while importFileContent.length() > 1:
		var lineEndIndicatorIndex = importFileContent.find("\n")
		line = importFileContent.substr(0, lineEndIndicatorIndex)
		exportFileContent += "\n// " + packagesfilePath + " without multiline-comment symbols: " + line.replace("*/", "").replace("/*", "")
		exportLineCount += 1
		if MAX_EXPORT_LINES > 0 && exportLineCount >= MAX_EXPORT_LINES:
			break
		importFileContent = importFileContent.substr(lineEndIndicatorIndex + 1)
		NextLine ()
	var exportFile = FileAccess.open(exportPath, FileAccess.WRITE)
	exportFile.store_string(exportFileContent)
	print("Done")

func NextLine ():
	for i in range(line.length()):
		var char = line[i]
		commentIndicatorIndex = line.find(COMMENT_INDICATOR, i)
		startMultilineCommentIndicatorIndex = line.find(MULTILINE_COMMENT_START_INDICATOR, i)
		endMultilineCommentIndicatorIndex = line.find(MULTILINE_COMMENT_END_INDICATOR, i)
		inCommentDict[i] = IsInComment(i)
	var defineIndicatorIndex = line.find(DEFINE_INDICATOR)
	if defineIndicatorIndex != -1 && !inCommentDict[defineIndicatorIndex]:
		var defineText = line.substr(defineIndicatorIndex + DEFINE_INDICATOR.length())
		var defineName = defineText
		var defineValue
		var macroArguementsStartIndicatorIndex = defineText.find(MACRO_ARGUMENTS_START_INDICATOR)
		var indexOfSpace = defineText.find(" ")
		if macroArguementsStartIndicatorIndex != -1:
			if indexOfSpace != -1:
				defineName = defineText.substr(0, mini(macroArguementsStartIndicatorIndex, indexOfSpace))
				defineValue = defineText.substr(indexOfSpace)
			else:
				defineName = defineText.substr(0, macroArguementsStartIndicatorIndex)
		elif indexOfSpace != -1:
			defineName = defineText.substr(0, indexOfSpace)
			defineValue = defineText.substr(indexOfSpace)
		if definesDict.has(defineName):
			exportFileContent += "\n" + UNDEFINE_GODOT_INDICATOR + defineName
			exportLineCount += 1
		else:
			definesDict[defineName] = defineValue
		exportFileContent += "\n" + line
		exportLineCount += 1
	else:
		var undefineIndicatorIndex = line.find(UNDEFINE_UNITY_INDICATOR)
		if undefineIndicatorIndex != -1 && !inCommentDict[undefineIndicatorIndex]:
			var defineText = line.substr(undefineIndicatorIndex + UNDEFINE_UNITY_INDICATOR.length())
			definesDict.erase(defineText)
			exportFileContent += "\n" + UNDEFINE_GODOT_INDICATOR + defineText
			exportLineCount += 1
		else:
			var includeStartIndicatorIndex = line.find(INCLUDE_START_INDICATOR)
			if includeStartIndicatorIndex != -1 && !inCommentDict[includeStartIndicatorIndex]:
				var includeFilePath = line.substr(includeStartIndicatorIndex + INCLUDE_START_INDICATOR.length())
				includeFilePath = packagesCachePath + "/" + includeFilePath
				var _includeFilePath = includeFilePath
				while !FileAccess.file_exists(includeFilePath):
					includeFilePath = includeFilePath.substr(0, includeFilePath.length() - 1)
					if includeFilePath.length() == 0:
						print(_includeFilePath + " doesn't exist")
						return
				if includes.has(includeFilePath):
					return
				includes.append(includeFilePath)
				packagesfilePath = includeFilePath.replace(packagesCachePath + "/", "")
				var includeFile = FileAccess.open(includeFilePath, FileAccess.READ)
				var includeFileContent = includeFile.get_as_text()
				importFileContent = importFileContent.insert(0, includeFileContent)
			else:
				var ifDefinedIndicatorIndex = line.find(IF_DEFINED_INDICATOR)
				if ifDefinedIndicatorIndex != -1 && !inCommentDict[ifDefinedIndicatorIndex]:
					var defineText = line.substr(ifDefinedIndicatorIndex + IF_DEFINED_INDICATOR.length())
					exportFileContent += "\n" + line
				else:
					var ifUndefinedIndicatorIndex = line.find(IF_UNDEFINED_INDICATOR)
					if ifUndefinedIndicatorIndex != -1 && !inCommentDict[ifUndefinedIndicatorIndex]:
						var defineText = line.substr(ifUndefinedIndicatorIndex + IF_UNDEFINED_INDICATOR.length())
						exportFileContent += "\n" + line
					else:
						var endIfIndicatorIndex = line.find(END_IF_INDICATOR)
						if endIfIndicatorIndex != -1 && !inCommentDict[endIfIndicatorIndex]:
							exportFileContent += "\n" + line
						else:
							var ifDirectiveIndicatorIndex = line.find(IF_DIRECTIVE_INDICATOR)
							if ifDirectiveIndicatorIndex != -1 && !inCommentDict[ifDirectiveIndicatorIndex]:
								#var statementAfterIfDirectiveIndicatorIndex = ifDirectiveIndicatorIndex + IF_DIRECTIVE_INDICATOR.length()
								#var statementAfterIfDirective = line.substr(statementAfterIfDirectiveIndicatorIndex)
								#var indexOfSpace = statementAfterIfDirective.find(" ")
								#if indexOfSpace != -1:
									#var wordAfterIfDirective = statementAfterIfDirective.substr(0, indexOfSpace).replace(OR_INDICATOR, "").replace(AND_INDICATOR, "")
									#line = IF_DIRECTIVE_INDICATOR + "(" + DEFINED_START_INDICATOR + wordAfterIfDirective + DEFINED_END_INDICATOR + " " + AND_INDICATOR + " " + wordAfterIfDirective + ") " + statementAfterIfDirective.substr(indexOfSpace)
									#while indexOfSpace != -1:
										#indexOfSpace = statementAfterIfDirective.find(" ", indexOfSpace + 1)
										#if indexOfSpace != -1:
											#wordAfterIfDirective = statementAfterIfDirective.substr(0, indexOfSpace)
											#statementAfterIfDirective = statementAfterIfDirective.substr(indexOfSpace + 1)
										#if !wordAfterIfDirective.begins_with(DEFINED_START_INDICATOR) && !wordAfterIfDirective.begins_with(TRUE_INDICATOR) && !wordAfterIfDirective.begins_with(FALSE_INDICATOR):
											#line = line.replace(wordAfterIfDirective, "(" + DEFINED_START_INDICATOR + wordAfterIfDirective + DEFINED_END_INDICATOR + " " + AND_INDICATOR + " " + wordAfterIfDirective + ")")
								#elif !statementAfterIfDirective.begins_with(DEFINED_START_INDICATOR) && !statementAfterIfDirective.begins_with(TRUE_INDICATOR) && !statementAfterIfDirective.begins_with(FALSE_INDICATOR):
									#line = IF_DIRECTIVE_INDICATOR + "(" + DEFINED_START_INDICATOR + statementAfterIfDirective + DEFINED_END_INDICATOR + " " + AND_INDICATOR + " " + statementAfterIfDirective + ")"
								exportFileContent += "\n" + line
								exportLineCount += 1
							else:
								var endIfDirectiveIndicatorIndex = line.find(DEFINE_INDICATOR)
								if endIfDirectiveIndicatorIndex != -1 && !inCommentDict[endIfDirectiveIndicatorIndex]:
									exportFileContent += "\n" + line
									exportLineCount += 1
								else:
									var elseDirectiveIndicator = line.find(ELSE_DIRECTIVE_INDICATOR)
									if elseDirectiveIndicator != -1 && !inCommentDict[elseDirectiveIndicator]:
										exportFileContent += "\n" + line
										exportLineCount += 1
									else:
										var elseIfDirectiveIndicatorIndex = line.find(ELSE_IF_DIRECTIVE_INDICATOR)
										if elseIfDirectiveIndicatorIndex != -1 && !inCommentDict[elseIfDirectiveIndicatorIndex]:
											exportFileContent += "\n" + line
											exportLineCount += 1
										else:
											var pragmaTargetIndicatorIndex = line.find(PRAGMA_TARGET_INDIDCATOR)
											var pragmaTarget = line.substr(pragmaTargetIndicatorIndex + PRAGMA_TARGET_INDIDCATOR.length())
											pragmaTarget = pragmaTarget.replace(".", "")
											if pragmaTargetIndicatorIndex != -1 && !inCommentDict[pragmaTargetIndicatorIndex]:
												exportFileContent += "\n" + UNDEFINE_GODOT_INDICATOR + "SHADER_TARGET\n" + DEFINE_INDICATOR + "SHADER_TARGET " + pragmaTarget
												exportFileContent += "\n" + UNDEFINE_GODOT_INDICATOR + "SHADER_AVAILABLE_RANDOMWRITE\n" + DEFINE_INDICATOR + "SHADER_AVAILABLE_RANDOMWRITE " + str(int(pragmaTarget) >= 45)
												#exportFileContent += "\n" + UNDEFINE_GODOT_INDICATOR + "SHADER_AVAILABLE_CUBEARRAY\n" + DEFINE_INDICATOR + "SHADER_AVAILABLE_CUBEARRAY " + str()
												exportLineCount += 2
		return line

func IsInComment (index : int):
	if startMultilineCommentIndicatorIndex != -1 && startMultilineCommentIndicatorIndex <= index:
		inComment = true
	if endMultilineCommentIndicatorIndex != -1 && endMultilineCommentIndicatorIndex <= index:
		inComment = false
	return inComment || (index >= commentIndicatorIndex && commentIndicatorIndex != -1)
	
func ExtractPropertyName ():
	var propertyNameStartIndicatorIndex = importFileContent.find(PROPERTY_NAME_START_INDICATOR)
	var propertyNameEndIndicatorIndex = importFileContent.find(PROPERTY_NAME_END_INDICATOR)
	var output = importFileContent.substr(propertyNameStartIndicatorIndex, propertyNameEndIndicatorIndex - propertyNameStartIndicatorIndex)
	importFileContent = importFileContent.substr(propertyNameEndIndicatorIndex + PROPERTY_NAME_END_INDICATOR.length())
	return output

func ExtractPropertyType ():
	var propertyTypeTextStartIndicatorIndex = importFileContent.find(PROPERTY_TYPE_START_INDICATOR) + PROPERTY_TYPE_START_INDICATOR.length()
	var propertyTypeTextEndIndicatorIndex = importFileContent.find(PROPERTY_TYPE_END_INDICATOR)
	var propertyTypeText = importFileContent.substr(propertyTypeTextStartIndicatorIndex, propertyTypeTextEndIndicatorIndex - propertyTypeTextStartIndicatorIndex)
	importFileContent = importFileContent.substr(propertyTypeTextEndIndicatorIndex + PROPERTY_TYPE_END_INDICATOR.length())
	if propertyTypeText == "2D":
		return Property.Type.Texture2D
	elif propertyTypeText == "Color" || propertyTypeText == "Vector":
		return Property.Type.Vector
	elif propertyTypeText == "Float":
		return Property.Type.Float

func ExtractPropertyValue (propertyType : Property.Type):
	var propertyValueStartIndicatorIndex = importFileContent.find(PROPERTY_VALUE_START_INDICATOR)
	var propertyValueEndIndicatorIndex = importFileContent.find(PROPERTY_VALUE_END_INDICATOR)
	importFileContent = importFileContent.substr(propertyValueStartIndicatorIndex + PROPERTY_VALUE_START_INDICATOR.length())
	var propertyValue
	if propertyType == Property.Type.Float:
		propertyValue = float(importFileContent.substr(propertyValueEndIndicatorIndex - propertyValueStartIndicatorIndex))
	elif propertyType == Property.Type.Vector:
		var propertyValueSeparatorIndicatorIndex = importFileContent.find(VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR)
		var x = float(importFileContent.substr(0, propertyValueSeparatorIndicatorIndex))
		importFileContent = importFileContent.substr(propertyValueSeparatorIndicatorIndex + VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR.length())
		propertyValueSeparatorIndicatorIndex = importFileContent.find(VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR)
		var y = float(importFileContent.substr(0, propertyValueSeparatorIndicatorIndex))
		importFileContent = importFileContent.substr(propertyValueSeparatorIndicatorIndex + VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR.length())
		propertyValueSeparatorIndicatorIndex = importFileContent.find(VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR)
		var z = float(importFileContent.substr(0, propertyValueSeparatorIndicatorIndex))
		importFileContent = importFileContent.substr(propertyValueSeparatorIndicatorIndex + VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR.length())
		propertyValueSeparatorIndicatorIndex = importFileContent.find(VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR)
		var w = float(importFileContent.substr(0, propertyValueSeparatorIndicatorIndex))
		importFileContent = importFileContent.substr(propertyValueSeparatorIndicatorIndex + VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR.length())
		propertyValueSeparatorIndicatorIndex = importFileContent.find(VECTOR_PROPERTY_COMPONENT_SEPARATOR_INDICATOR)
		propertyValue = Vector4(x, y, z, w)
	elif propertyType == Property.Type.Texture2D:
		propertyValue = null
	importFileContent = importFileContent.substr(propertyValueEndIndicatorIndex + PROPERTY_VALUE_END_INDICATOR.length())
	return propertyValue

class Property:
	var name : String
	var type : Type
	var value
	
	static func New (name : String, type : Type, value):
		var output = Property.new()
		output.name = name
		output.type = type
		output.value = value
		return output
	
	enum Type {
		Texture2D,
		Float,
		Vector
	}
