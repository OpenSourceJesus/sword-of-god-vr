extends Node
class_name BulletPattern

@export var canShoot : bool
@export var changesSpawnNodeTransformOnShoot : bool

func Init (spawnNode : Node3D):
	pass

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	if !canShoot || spawnNode ==  null:
		return []
	var output : Array[Bullet]
	var bullet = load(bulletScenePath).instantiate()
	output.append(bullet)
	bullet.global_transform = spawnNode.global_transform
	bullet.partOfBulletPattern = self
	Room.instance.add_child(bullet)
	return output
