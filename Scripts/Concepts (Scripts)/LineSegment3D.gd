extends Node3D
class_name LineSegment3D

@export var start : Vector3
@export var end : Vector3

static func New (start : Vector3, end : Vector3) -> LineSegment3D:
	var output = new()
	output.start = start
	output.end = end
	return output

func ToString ():
	return '[' + str(start) + '], [' + str(end) + ']'

func ContainsPoint (point : Vector3):
	return (point - start).length() + (point - end).length() == (start - end).length()

func Move (movement : Vector3):
	return New(start + movement, end + movement)

func Rotate (pivotPoint : Vector3, rotation : Quaternion) -> LineSegment3D:
	var outputStart = VectorExtensions.RotatedAroundPivot3D(start, pivotPoint, rotation)
	var outputEnd = VectorExtensions.RotatedAroundPivot3D(end, pivotPoint, rotation)
	return New(outputStart, outputEnd)

#func ClosestPoint (point : Vector3):
	#var output : Vector3
	#var directedDistanceAlongParallel = GetDirectedDistanceAlongParallel(point)
	#if directedDistanceAlongParallel > 0 && directedDistanceAlongParallel < GetLength():
		#output = GetPointWithDirectedDistance(directedDistanceAlongParallel)
	#elif directedDistanceAlongParallel >= GetLength():
		#output = end
	#else:
		#output = start
	#return output

func GetMidpoint () -> Vector3:
	return (start + end) / 2

#func GetDirectedDistanceAlongParallel (point : Vector3) -> float:
	#var rotation = Quaternion.LookRotation(end - start)
	#rotation = Quaternion.Inverse(rotation)
	#var rotatedLine = Rotate(Vector3.zero, rotation)
	#point = rotation * point
	#return point.z - rotatedLine.start.z

func GetPointWithDirectedDistance (directedDistance : float) -> Vector3:
	return start + (GetDirection() * directedDistance)

func GetLength () -> float:
	return (start - end).length()

func GetLengthSqr () -> float:
	return (start - end).length_squared()

func GetDirection () -> Vector3:
	return (end - start).normalized()

func Flip () -> LineSegment3D:
	return New(end, start)

#func DoIIntersectWithSphere (Sphere sphere) -> bool:
	#return (ClosestPoint(sphere.center) - sphere.center).length_squared() <= sphere.radius * sphere.radius

#func DoIIntersectWithLineSegment (other : LineSegment3D, shouldIncludeEndPoints : bool) -> bool:
	#pass
