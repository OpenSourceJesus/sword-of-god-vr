extends Node3D
class_name FaceCamera

func _process (deltaTime : float):
	look_at(NodeExtensions.GetNode(Camera3D).global_position)
