extends Node
class_name Renderer

@export var meshInstance : MeshInstance3D
@export var setInitTimeOffset : bool
@export var initTimeOffsetRange : FloatRange
static var instances : Array[Renderer]

func _ready ():
	instances.append(self)
	tree_exiting.connect(OnTreeExiting)
	if setInitTimeOffset && meshInstance != null:
		var shaderMaterial = meshInstance.material as ShaderMaterial
		if shaderMaterial != null:
			shaderMaterial = shaderMaterial.duplicate()
			shaderMaterial.set_shader_parameter('timeOffset', randf_range(initTimeOffsetRange.min, initTimeOffsetRange.max))
			meshInstance.material = shaderMaterial

func OnTreeExiting ():
	instances.erase(self)
