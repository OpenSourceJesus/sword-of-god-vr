extends Node
class_name GodEffect

@export var isBad : bool
@export var pointsRequired : int
@export var scalesWithPoints : bool
@export var subtractPointsRequired : bool

func CanBeApplied (points : int):
	var output
	if isBad:
		output = points <= pointsRequired
	else:
		output = points >= pointsRequired
	return output

func Apply (points : int):
	if !Player.instance.godEffectsDict[get_parent().get_parent()].has(self):
		Player.instance.godEffectsDict[get_parent().get_parent()].append(self)

func Unapply (points : int):
	Player.instance.godEffectsDict[get_parent().get_parent()].erase(self)

func GetDescription ():
	return name
