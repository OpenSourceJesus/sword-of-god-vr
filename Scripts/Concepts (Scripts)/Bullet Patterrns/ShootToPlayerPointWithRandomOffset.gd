extends BulletPattern
class_name ShootToPlayerPointWithRandomOffset

@export var randomOffsetRange : FloatRange

func Shoot (bulletScenePath : String, spawnNode : Node2D):
	var output : Array[Bullet]
	var point = Player.instance.global_position
	point += VectorExtensions.Random(randomOffsetRange)
	var bullet = load(bulletScenePath).instantiate()
	output.append(bullet)
	bullet.global_transform = spawnNode.global_transform
	bullet.look_at(point)
	if bullet.despawnMode == Bullet.DespawnMode.Time:
		bullet.duration = (point - bullet.rigidBody.global_position).length() / bullet.moveSpeed
	elif bullet.despawnMode == Bullet.DespawnMode.Range:
		bullet.range = (point - bullet.rigidBody.global_position).length()
	get_tree().current_scene.add_child(bullet)
	return output
