extends BulletPattern
class_name LoopBulletPatterns

@export var count : int
@export var bulletPatterns : Array[BulletPattern]

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	var output : Array[Bullet]
	for i in range(count):
		for bulletPattern in bulletPatterns:
			output.append_array(bulletPattern.Shoot(bulletScenePath, spawnNode))
	return output
