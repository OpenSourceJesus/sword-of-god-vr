extends ShootAheadWithOffset
class_name ShootAheadWithOffsetAndContinuouslySplitWithCountEachTimeIntervalInRelativeArc

@export var interval : float
@export var splitArcRange : FloatRange
@export var splitCount : int
@export var splitBulletScenePath : String

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	var output = super.Shoot(bulletScenePath, spawnNode)
	for bullet in output:
		bullet.set_meta("timeTillSplit", interval)
		bullet.callOnUnpausedFrame = OnUnpausedFrame
	return output

func OnUnpausedFrame (bullet : Bullet):
	var timeTillSplit = bullet.get_meta("timeTillSplit")
	timeTillSplit -= Player.instance.deltaTime
	if timeTillSplit <= 0:
		timeTillSplit += interval
		var splitAngle = splitArcRange.min
		for i in range(splitCount):
			var splitBullet = load(splitBulletScenePath).instantiate()
			splitBullet.global_transform = bullet.rigidBody.global_transform
			splitBullet.global_rotation_degrees += splitAngle
			Room.instance.add_child(splitBullet)
			splitAngle += (splitArcRange.max - splitArcRange.min) / (splitCount - 1)
	bullet.set_meta("timeTillSplit", timeTillSplit)
