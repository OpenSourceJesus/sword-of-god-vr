extends BulletPattern
class_name ShootAheadWithRandomOffset

@export var randomOffsetRange : FloatRange

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	var output : Array[Bullet]
	var bullet = load(bulletScenePath).instantiate()
	output.append(bullet)
	get_tree().current_scene.add_child(bullet)
	bullet.global_transform = spawnNode.global_transform
	bullet.global_rotate(VectorExtensions.Random3D(FloatRange.New(1, 1)), randf_range(randomOffsetRange.min, randomOffsetRange.max))
	return output
