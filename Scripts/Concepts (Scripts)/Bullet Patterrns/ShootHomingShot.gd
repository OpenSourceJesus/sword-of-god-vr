extends BulletPattern
class_name ShootHomingShot

@export var rotateSpeed : float

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	var bullets = super.Shoot(bulletScenePath, spawnNode)
	for bullet in bullets:
		bullet.callOnUnpausedFrame = OnUnpausedFrame
	return bullets

func OnUnpausedFrame (bullet : Bullet):
	var toPlayer = Player.instance.characterBody.global_position - bullet.rigidBody.global_position
	var heading = VectorExtensions.RotatedTo3D(bullet.rigidBody.linear_velocity.normalized(), toPlayer.normalized(), rotateSpeed * Player.instance.deltaTime)
	bullet.rigidBody.look_at(bullet.rigidBody.global_position + heading)
