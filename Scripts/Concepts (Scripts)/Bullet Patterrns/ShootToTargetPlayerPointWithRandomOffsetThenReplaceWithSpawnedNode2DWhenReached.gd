extends BulletPattern
class_name ShootToTargetPlayerPointWithRandomOffsetThenReplaceWithSpawnedNode2DAfterDuration

@export var randomOffsetRange : FloatRange
@export var stayStillDuration : float
@export var node2DScenePath : String

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	var output : Array[Bullet]
	var point = Player.instance.global_position
	point += VectorExtensions.Random3D(randomOffsetRange)
	var bullet = load(bulletScenePath).instantiate()
	output.append(bullet)
	bullet.global_transform = spawnNode.global_transform
	bullet.look_at(point)
	bullet.despawnMode = Bullet.DespawnMode.Time
	bullet.duration = (point - bullet.rigidBody.global_position).length() / bullet.moveSpeed + stayStillDuration
	bullet.callOnUnpausedFrame = Callable(self, 'OnBulletUpdate')
	get_tree().current_scene.add_child(bullet)
	return output
	
func OnBulletUpdate (bullet : Bullet):
	if bullet.duration <= stayStillDuration:
		bullet.moveSpeed = 0
		bullet.callOnUnpausedFrame = Callable()
		bullet.callOnDestroy = Callable(self, 'OnBulletDestroy')

func OnBulletDestroy (bullet : Bullet):
	var node = load(node2DScenePath).instantiate()
	node.global_transform = bullet.rigidBody.global_transform
	if get_tree() != null:
		get_tree().current_scene.add_child(node)
