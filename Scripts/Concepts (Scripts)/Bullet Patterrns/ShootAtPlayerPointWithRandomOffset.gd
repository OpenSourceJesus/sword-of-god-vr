extends BulletPattern
class_name ShootAtPlayerPointWithRandomOffset

@export var randomOffsetRange : FloatRange

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	var output : Array[Bullet]
	var point = Player.instance.global_position
	point += VectorExtensions.Random(randomOffsetRange)
	var bullet = load(bulletScenePath).instantiate()
	output.append(bullet)
	bullet.global_transform = spawnNode.global_transform
	bullet.look_at(point)
	get_tree().current_scene.add_child(bullet)
	return output
