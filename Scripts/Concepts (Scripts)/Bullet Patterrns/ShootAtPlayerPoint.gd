extends BulletPattern
class_name ShootAtPlayerPoint

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	var output : Array[Bullet]
	if canShoot:
		var bullet = load(bulletScenePath).instantiate()
		output.append(bullet)
		Room.instance.add_child(bullet)
		if changesSpawnNodeTransformOnShoot:
			spawnNode.look_at(Player.instance.characterBody.global_position)
			bullet.global_position = spawnNode.global_position
		else:
			bullet.global_position = spawnNode.global_position
			bullet.look_at(Player.instance.characterBody.global_position)
	elif changesSpawnNodeTransformOnShoot:
		spawnNode.look_at(Player.instance.characterBody.global_position)
	return output
