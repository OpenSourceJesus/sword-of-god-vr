extends BulletPattern
class_name RotateToAndShootAtLocalAngle

@export var degrees : Vector3

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	spawnNode.rotation_degrees = degrees
	return super.Shoot(bulletScenePath, spawnNode)
