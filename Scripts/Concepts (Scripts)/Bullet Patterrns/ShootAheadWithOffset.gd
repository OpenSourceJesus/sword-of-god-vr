extends BulletPattern
class_name ShootAheadWithOffset

@export var offset : Vector3

func Shoot (bulletScenePath : String, spawnNode : Node3D):
	spawnNode.global_rotation_degrees += offset
	var output = super.Shoot(bulletScenePath, spawnNode)
	spawnNode.global_rotation_degrees -= offset
	return output
