extends Node3D
class_name Destructable

@export var maxHp : float
var hp : float
var dead = false

func _ready ():
	hp = maxHp

func TakeDamage (amount : float):
	hp = clampf(hp - amount, 0, maxHp)
	if !dead && hp == 0:
		Die ()

func Die ():
	dead = true
	queue_free()
