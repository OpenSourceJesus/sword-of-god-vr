extends Resource
class_name _Shape2D

@export var corners : PackedVector2Array
@export var edges : Array

static func From_Shape2D (shape : _Shape2D):
	var output = _Shape2D.new()
	output.corners.append_array(shape.corners)
	output.edges.append_array(shape.edges)
	return output

func SetCornersOfPolygon ():
	corners.clear()
	for edge in edges:
		corners.append(edge.end)

func SetEdgesOfPolygon ():
	edges.clear()
	var previousCorner = corners[corners.size() - 1]
	for i in range(corners.size()):
		var corner = corners[i]
		edges.append(LineSegment2D.New(previousCorner, corner))
		previousCorner = corner

func GetPerimeter ():
	var output = 0.0
	for edge in edges:
		output += edge.GetLength()
	return output

func GetPointOnPerimeter (distance : float):
	var perimeter = GetPerimeter()
	while true:
		for i in range(edges.size()):
			var edge = edges[i]
			var edgeLength = edge.GetLength()
			distance -= edgeLength
			if distance <= 0:
				return edge.GetPointWithDirectedDistance(edgeLength + distance)

func ContainsForPolygon (point : Vector2, equalPointsIntersect = true, checkDistance = 99999.0):
	var checkLineSegment = LineSegment2D.New(point, point + (VectorExtensions.Random2D() * checkDistance))
	var collisionCount = 0
	for i in range(edges.size()):
		var edge = edges[i]
		if edge.DoIIntersectWith(checkLineSegment, equalPointsIntersect):
			collisionCount += 1
	return collisionCount % 2 == 1

func CrossesOverSelf ():
	for i in range(edges.size()):
		var edge = edges[i]
		for i2 in range(i + 1, edges.size()):
			var edge2 = edges[i2]
			if edge.DoIIntersectWith(edge2, false):
				return true
	return false

func DoIIntersectWithLineSegment2D (lineSegment : LineSegment2D, equalPointsIntersect = true):
	for i in range(edges.size()):
		var edge = edges[i]
		if edge.DoIIntersectWith(lineSegment, equalPointsIntersect):
			return true
	return false

func GetRandomPoint (checkIfContained = true, containsEdges = true, checkDistance = 99999.0):
	var perimeter = GetPerimeter()
	while true:
		var point1 = GetPointOnPerimeter(randf_range(0, perimeter))
		var point2 = GetPointOnPerimeter(randf_range(0, perimeter))
		var output = (point1 + point2) / 2
		if !checkIfContained || ContainsForPolygon(output, containsEdges, checkDistance):
			return output

func GetClosestPoint (point : Vector2, checkDistance = 99999.0):
	return GetClosestPointAndDistanceSqr(point, checkDistance)[0]

func GetDistanceSqr (point : Vector2, checkDistance = 99999.0):
	return GetClosestPointAndDistanceSqr(point, checkDistance)[1]

func GetClosestPointAndDistanceSqr (point : Vector2, checkDistance = 99999.0):
	if ContainsForPolygon(point, true, checkDistance):
		return [ point, 0 ]
	else:
		var closestPoint = Vector2()
		var closestDistanceSqr = INF
		for i in range(edges.size()):
			var edge = edges[i]
			var pointOnPerimeter = edge.GetClosestPoint(point)
			var distanceSqr = (point - pointOnPerimeter).length_squared()
			if distanceSqr < closestDistanceSqr:
				closestDistanceSqr = distanceSqr
				closestPoint = pointOnPerimeter
		return [ closestPoint, closestDistanceSqr ]

func DoIIntersectWithPolygon (shape : _Shape2D, equalPointsIntersect = true, checkDistance = 99999):
	for i in range(edges.size()):
		var edge = edges[i]
		if shape.DoIIntersectWith(edge, equalPointsIntersect):
			return true
	return ContainsForPolygon(corners[0], equalPointsIntersect, checkDistance)

func Subdivided ():
	var output : Array[LineSegment2D]
	for i in range(edges.size()):
		var edge = edges[i]
		var midPoint = edge.GetMidpoint()
		output.append(LineSegment2D.New(edge.start, midPoint))
		output.append(LineSegment2D.New(midPoint, edge.end))
	return PolygonFromEdges(output)

func GetIntersections (shape : _Shape2D, cornersCanIntersect = false):
	var output : PackedVector2Array
	for edge in edges:
		for edge2 in shape.edges:
			var intersectionPoint = edge.GetIntersectionWith(edge2, true)
			if intersectionPoint != null && (cornersCanIntersect || !corners.has(intersectionPoint)):
				output.append(intersectionPoint)
	return output

#func IntersectionOfConvexPolygonForConvexPolygon (shape : _Shape2D, checkDistance : float = 99999.0):
	#var outputCorners = GetIntersections(shape)
	#if outputCorners.size() == 0:
		#if ContainsForPolygon(shape.corners[0], true, checkDistance):
			#return PolygonFromEdges(shape.edges)
		#elif shape.ContainsForPolygon(self.corners[0], true, checkDistance):
			#return PolygonFromEdges(edges)
	#else:
		#for corner in corners:
			#if shape.ContainsForPolygon(corner, true, checkDistance) && !outputCorners.has(corner):
				#outputCorners.append(corner)
		#for corner in shape.corners:
			#if ContainsForPolygon(corner, true, checkDistance) && !outputCorners.has(corner):
				#outputCorners.append(corner)
		#var uniquePermutations = ArrayExtensions.UniquePermutations(outputCorners)
		#for uniquePermutation in uniquePermutations:
			#var output = PolygonFromCorners(uniquePermutation.ToArray())
			#if output.ContainsTheSameEdgeDirectionsAsGroup(self, shape):
				#return output
	return null

func BooleanedForPolygon (toggle : _Shape2D):
	push_error("_Shape2D.BooleanForPolygon() is not made")

#func RemovedForPolygon (remove : _Shape2D):
	#var corners : PackedVector2Array
	#corners.append_array(self.corners)
	#corners.append_array(remove.corners)
	#var intersection = IntersectionOfConvexPolygonForConvexPolygon(remove)
	#if intersection == null:
		#return self
	#corners = ArrayExtensions.RemoveEach(corners, intersection.corners)
	#var uniquePermutations = ArrayExtensions.UniquePermutations(corners)
	#for uniquePermutation in uniquePermutations:
		#var output = PolygonFromCorners(uniquePermutation.ToArray())
		#if output.ContainsTheSameEdgeDirectionsAsGroup(self, remove):
			#return output
	#return self

func SmoothForPolygon ():
	var outputCorners : PackedVector2Array
	for edge in edges:
		outputCorners.append(edge.GetMidpoint())
	return PolygonFromCorners(outputCorners)

func GrowFromForPolygon (amount : float, point : Vector2):
	var outputCorners : PackedVector2Array
	for corner in corners:
		var fromPoint = corner - point
		outputCorners.append(corner + fromPoint.normalized * (fromPoint.magnitude + amount))
	return PolygonFromCorners(outputCorners)

#func ShrinkToForPolygon (amount : float, point : Vector2, shrinkPastPoint = false):
	#var outputCorners : PackedVector2Array
	#for corner in corners:
		#var toPoint = point - corner
		#if shrinkPastPoint:
			#outputCorners.append(corner + toPoint.normalized * (toPoint.magnitude + amount))
		#else:
			#var newCorner = corner + toPoint.normalized * (toPoint.magnitude + amount)
			#if point - corner.dot(point - newCorner) >= 0:
				#outputCorners.append(newCorner)
	#return PolygonFromCorners(outputCorners)

func Interpolated (to : _Shape2D, normalizedAmount : float):
	var outputCorners : PackedVector2Array
	for i in range(corners.size()):
		var corner = corners[i]
		var toCorner = to.corners[i]
		outputCorners.append(corner.lerp(toCorner, normalizedAmount))
	return PolygonFromCorners(outputCorners)

#func MergedForPolygon (shape : _Shape2D):
	#var corners : PackedVector2Array
	#corners.append_array(self.corners)
	#corners.append_array(shape.corners)
	#var uniquePermutations = ArrayExtensions.UniquePermutations(corners)
	#for uniquePermutation in uniquePermutations:
		#var output = PolygonFromCorners(uniquePermutation)
		#if output.ContainsTheSameEdgeDirectionsAsGroup(self, shape):
			#return output
	#return null

func CollapseDuplicateCornersForPolygon ():
	var output = From_Shape2D(self)
	var pastCorners : PackedVector2Array
	for i in range(corners.size()):
		var corner = corners[i]
		if pastCorners.has(corner):
			output = output.RemoveEdgeForPolygon(i)
		else:
			pastCorners.append(corner)
	return output

func RemoveEdgeForPolygon (edgeIndex : int):
	edges.remove_at(edgeIndex)
	return PolygonFromEdges(edges)

func InsertCornerForPolygon (cornerIndex : int, corner : Vector2):
	corners.insert(cornerIndex, corner)
	return PolygonFromCorners(corners)

func InsertCornerBetweenClosestEdgeForPolygon (corner : Vector2):
	var closestEdgeIndex = 0
	var closestEdgeDistanceSqr = edges[0].GetDistanceSqrTo(corner)
	#for (int i = 1 i < edges.size() i += 1)
	for i in range(1, edges.size()):
		var edge = edges[i]
		var distanceSqr = edge.GetDistanceSqrTo(corner)
		if distanceSqr < closestEdgeDistanceSqr:
			closestEdgeIndex = i
			closestEdgeDistanceSqr = distanceSqr
	return InsertCornerForPolygon(closestEdgeIndex, corner)

func ContainsTheSameEdgeDirectionsAsGroup (shapes : Array[_Shape2D]):
	var correctDirections : PackedVector2Array
	for shape in shapes:
		for edge in shape.edges:
			correctDirections.append(edge.GetDirection())
	for edge in edges:
		if !correctDirections.has(edge.GetDirection()):
			return false
	return true

func GetCornerAverage ():
	var output = Vector2()
	for corner in corners:
		output += corner
	return output / corners.size()

func GetCenter ():
	var min = corners[0]
	var max = min
	for i in range(1, corners.size()):
		var corner = corners[i]
		min = VectorExtensions.Min(corner, min)
		max = VectorExtensions.Max(corner, max)
	return (min + max) / 2

func GetClosestCornerIndex (point : Vector2, checkEdges = false):
	if !checkEdges:
		return VectorExtensions.GetIndexOfClosestPoint(point, corners)
	else:
		var closestEdgeIndex = 0
		var closestDistanceSqr = INF
		for i in range(edges.size()):
			var edge = edges[i]
			var pointOnPerimeter = edge.GetClosestPoint(point)
			var distanceSqr = (point - pointOnPerimeter).sqrMagnitude
			if distanceSqr < closestDistanceSqr:
				closestDistanceSqr = distanceSqr
				if i == 0:
					closestEdgeIndex = edges.size() - 1
				else:
					closestEdgeIndex = i - 1
		return closestEdgeIndex

func GetDistanceSqrFrom_Shape2D (shape : _Shape2D):
	var output = INF
	for edge in edges:
		for edge2 in shape.edges:
			output = minf(edge.GetDistanceSqr(edge2), output)
	return output

func GetDraggedForPolygon (drag : Vector2):
	push_error("_Shape2D.GetDraggedForPolygon() is not made")
	var output = From_Shape2D(self)
	var dragLineSegment = LineSegment2D.new()
	var shapeAfterDrag = output.Move(drag)
	
	return output

func Moved (move : Vector2):
	var outputCorners : PackedVector2Array
	for corner in corners:
		outputCorners.append(corner + move)
	return PolygonFromCorners(outputCorners)

#func Transform (trs : Transform2D):
	#return PolygonFromCorners(trs * corners)

#func InverseTransform (trs : Transform2D)
	#Vector2[] outputCorners = new Vector2[corners.size()]
	#for (int i = 0 i < outputCorners.size() i += 1)
		#outputCorners[i] = trs.InverseTransformPoint(corners[i])
	#return PolygonFromCorners(outputCorners)

#func GetFurthestCorner (point : Vector2):
	#return VectorExtensions.GetFurthestPoint(point, corners)

#func ToSpline (spline : ):
	#for (int i = 0 i < corners.size() i += 1)
		#Vector2 corner = corners[i]
		#if (i >= spline.GetPointCount())
			#spline.InsertPointAt(spline.GetPointCount(), corner)
		#else
			#spline.SetPosition(i, corner)
	#for (int i = corners.size() i < spline.GetPointCount() i += 1)
		#spline.RemovePointAt(i)
		#i -= 1

#static func FromSpline (Spline spline)
#{
	#Vector2[] corners = new Vector2[spline.GetPointCount()]
	#for (int i = 0 i < corners.size() i += 1)
		#corners[i] = spline.GetPosition(i)
	#return PolygonFromCorners(corners)
#}

static func PolygonFromEdges (edges : Array[LineSegment2D]):
	var output = _Shape2D.new()
	output.edges.clear()
	output.edges.append_array(edges)
	output.SetCornersOfPolygon ()
	return output

static func PolygonFromCorners (corners : PackedVector2Array):
	var output = _Shape2D.new()
	output.corners.append_array(corners)
	output.SetEdgesOfPolygon ()
	return output

static func Polygon (edgeCount : float, rotation = 0.0, radius = 0.5):
	push_error("_Shape2D.Polygon(edgeCount : float, rotation = 0.0, radius = 0.5) is not made")

static func RegularPolygon (edgeCount : int, rotation : float = 0.0, radius : float = 0.5):
	var outputCorners : PackedVector2Array
	for i in range(edgeCount + 1):
		outputCorners.append(VectorExtensions.FromFacingAngle(360.0 / edgeCount * i) * radius)
	return PolygonFromCorners(outputCorners)

#static func Equals (shape : _Shape2D, shape2 : _Shape2D):
	#if shape.corners.size() != shape2.corners.size() || shape.edges.size() != shape2.edges.size():
		#return false
	#var firstCorner = shape.corners[0]
	#var indexOffset = 0
	#for i in range(shape2.corners.size()):
		#var corner = shape2.corners[i]
		#if corner == firstCorner:
			#indexOffset = i
			#break
	#for corner in corners:
		#if corner != shape2.corners[(i + indexOffset) % shape.corners.size()]:
			#return false
	#return true
#
#static func NotEquals (shape : _Shape2D, shape2 : _Shape2D):
	#if shape.corners.size() != shape2.corners.size() || shape.edges.size() != shape2.edges.size():
		#return true
	#var firstCorner = shape.corners[0]
	#var indexOffset = 0
	#for i in range(shape2.corners.size()):
		#var corner = shape2.corners[i]
		#if corner == firstCorner:
			#indexOffset = i
			#break
	#for i in range(shape.corners.size()):
		#var corner = shape.corners[i]
		#if corner != shape2.corners[(i + indexOffset) % shape.corners.size()]:
			#return true
	#return false
