extends Node
class_name GameManager

static var currentLevelSceneIndex = 0
static var instance : GameManager
static var paused : bool
static var initialized : bool
static var config = ConfigFile.new()
static var loading = false
static var justEnteredLevel = false
const SAVE_FILE_PATH = 'user://Save Data.cfg'

func _ready ():
	#SaveSceneAtPath ('res://Scenes/Concepts (Scenes)/UI Items (Scenes)/Room Indicator.tscn', 'res://Scenes/Concepts (Scenes)/UI Items (Scenes)/Room Indicator 2.tscn')
	#SaveSceneAtPath ('res://Scenes/Objects (Scenes)/Room.tscn', 'res://Scenes/Objects (Scenes)/Room 2.tscn')
	#SaveSceneAtPath ('res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 1.tscn', 'res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 1 2.tscn')
	#SaveSceneAtPath ('res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 2.tscn', 'res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 2 2.tscn')
	#SaveSceneAtPath ('res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 3.tscn', 'res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 3 2.tscn')
	#SaveSceneAtPath ('res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 4.tscn', 'res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 4 2.tscn')
	#SaveSceneAtPath ('res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 5.tscn', 'res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 5 2.tscn')
	#SaveSceneAtPath ('res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 6.tscn', 'res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 6 2.tscn')
	if instance == null:
		instance = self
	if !initialized:
		initialized = true
		Load ()
	elif loading:
		loading = false
		Load ()
	paused = false

static func NextLevel ():
	currentLevelSceneIndex += 1
	var scenePath = 'res://Scenes/' + str(currentLevelSceneIndex) + '.tscn'
	if !ResourceLoader.exists(scenePath):
		currentLevelSceneIndex -= 1
		return
	SetScene (scenePath)

static func PreviousLevel ():
	currentLevelSceneIndex -= 1
	var scenePath = 'res://Scenes/' + str(currentLevelSceneIndex) + '.tscn'
	if !ResourceLoader.exists(scenePath):
		currentLevelSceneIndex += 1
		return
	SetScene (scenePath)

static func SetLevel (levelSceneIndex : int):
	currentLevelSceneIndex = levelSceneIndex
	SetScene ('res://Scenes/' + str(levelSceneIndex) + '.tscn')

static func SetScene (scenePath : String):
	instance.get_tree().change_scene_to_file(scenePath)

static func AddScene (scenePath : String):
	var scene = load(scenePath)
	var clone = scene.instantiate()
	instance.get_tree().current_scene.add_child(clone)

func OnSetScene ():
	if get_tree() == null:
		return
	#Player.instance = %Player
	#_Camera2D.instance = %_Camera2D

static func SaveNode (rootNode = null, resourcePath = ''):
	if rootNode == null:
		rootNode = instance.get_tree().current_scene
	var scene = PackedScene.new()
	var error = scene.pack(rootNode)
	if !error:
		print('Packed scene')
		error = ResourceSaver.save(scene, resourcePath)
		if !error:
			print('Saved scene')
		else:
			print(error)
	else:
		print(error)

func SaveSceneAtPath (scenePath : String, resourcePath = ''):
	SaveNode (load(scenePath).instantiate(), resourcePath)

func Save ():
	config.set_value('', 'level', currentLevelSceneIndex)
	OnSetScene ()
	SetConfigValue ('', 'highscore', Player.highscore)
	config.save(SAVE_FILE_PATH)
	justEnteredLevel = false

func Load ():
	var error = config.load(SAVE_FILE_PATH)
	if error == OK:
		var levelIndex = config.get_value('', 'level', 0)
		if levelIndex == null:
			return
		if currentLevelSceneIndex != levelIndex:
			loading = true
			SetLevel (levelIndex)
			return
		OnSetScene ()
		Player.highscore = GetConfigValue('', 'highscore', Player.highscore)
	if justEnteredLevel:
		Save ()
	justEnteredLevel = false

func _notification (notification):
	if notification == NOTIFICATION_WM_CLOSE_REQUEST:
		Quit ()

func Quit ():
	Save ()
	get_tree().quit()

func ResetScene ():
	get_tree().reload_current_scene()

static func SetPaused (pause : bool):
	paused = pause

static func SetConfigValue (section : String, key : String, value):
	config.set_value(section, key, value)
	if justEnteredLevel:
		config.set_value('reset', key, value)

static func GetConfigValue (section : String, key : String, defaultValue = null):
	if justEnteredLevel:
		return config.get_value('reset', key, defaultValue)
	else:
		return config.get_value(section, key, defaultValue)
